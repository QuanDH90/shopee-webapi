﻿namespace Models.Filter;

public class NewFilter : FilterBase
{
    public string? CategoryId { get; set; } = null!;

    public string? Title { get; set; } = null!;

    public string? Slug { get; set; } = null!;

    public string? Description { get; set; }

    public string? Content { get; set; }

    public string? Tags { get; set; }
}
