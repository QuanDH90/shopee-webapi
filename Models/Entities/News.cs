﻿namespace Models.Entities;

public class News : BaseEntity
{
    public string CategoryId { get; set; } = null!;

    public string? CategoryName { get; set; }

    public string Title { get; set; } = null!;

    public string Slug { get; set; } = null!;

    public string? Image { get; set; }

    public string? Description { get; set; }

    public string? Content { get; set; }

    public string? Tags { get; set; }
}
