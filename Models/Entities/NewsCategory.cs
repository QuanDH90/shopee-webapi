﻿namespace Models.Entities;

public class NewsCategory : BaseEntity
{
    public string? ParentId { get; set; }

    public string Name { get; set; } = null!;

    public string Slug { get; set; } = null!;

    public string? Image { get; set; }
}
