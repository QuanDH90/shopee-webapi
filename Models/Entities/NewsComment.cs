﻿namespace Models.Entities;

public class NewsComment : BaseEntity
{
    public string NewsId { get; set; } = null!;

    public string NewsTitle { get; set; } = null!;

    public string? CommentId { get; set; }

    public string? CommentParentId { get; set; }

    public string? CommentTime { get; set; }

    public string? CommentContent { get; set; }

    public string? UserId { get; set; }

    public string? FullName { get; set; }

    public string? Email { get; set; }
}
