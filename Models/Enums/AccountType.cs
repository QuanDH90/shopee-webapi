﻿namespace Models.Enums;

public enum AccountType
{
	Customer = 0,
	Admin = 1
}
