﻿using Models.Common;
using Models.Entities;
using Models.Filter;

namespace WebApi.Services.Abstractions;

public interface INewsService : IBaseService<News>
{
    /// <summary>
    /// CreateOrUpdateAsync
    /// </summary>
    /// <param name="entity"></param>
    /// <returns></returns>
    Task<bool> CreateOrUpdateAsync(News entity);

    /// <summary>
    /// FilterDataPagingAsync
    /// </summary>
    /// <param name="filter"></param>
    /// <returns></returns>
    Task<DataPaging<News>?> FilterDataPagingAsync(NewFilter filter);
}
