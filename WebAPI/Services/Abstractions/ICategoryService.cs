﻿using Models.Common;
using Models.Entities;
using Models.Filter;
using Models.ViewModels;
using WebApi.Services.Abstractions;

namespace WebApi.Services.Abstractions;

/// <summary>
/// ICategoryService
/// </summary>
public interface ICategoryService : IBaseService<Category>
{
    /// <summary>
    /// GetActiveCategories
    /// </summary>
    /// <returns></returns>
    Task<IEnumerable<CategoryViewModel>?> GetActiveCategories();

    /// <summary>
    /// GetActiveCategories
    /// </summary>
    /// <returns></returns>
    Task<IEnumerable<CategoryViewModel>?> GetParentActiveCategories();

    /// <summary>
    /// GetShowingCategories
    /// </summary>
    /// <returns></returns>
    Task<IEnumerable<CategoryViewModel>?> GetShowingCategories();

    /// <summary>
    /// FilterDataPaging
    /// </summary>
    /// <param name="filter"></param>
    /// <returns></returns>
    Task<DataPaging<Category>?> FilterDataPaging(CategoryFilter filter);

    /// <summary>
    /// GetCategoryTree
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    Task<CategoryViewModel?> GetCategoryTree(string id);

    /// <summary>
    /// GetCategoryTreeBySlug
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    Task<CategoryViewModel?> GetCategoryTreeBySlug(string slug);

    /// <summary>
    /// GetChildrenCategoryByParentId
    /// </summary>
    /// <param name="parentId"></param>
    /// <returns></returns>
    Task<IEnumerable<Category>?> GetChildrenCategoryByParentId(string parentId);
}
