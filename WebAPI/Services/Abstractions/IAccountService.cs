﻿using Models.Entities;
using Models.Request;
using Models.Response;
using WebApi.Services.Abstractions;

namespace WebApi.Services.Abstractions;

public interface IAccountService : IBaseService<Account>
{
	/// <summary>
	/// AccountLogin
	/// </summary>
	/// <param name="request"></param>
	/// <returns></returns>
	Task<LoginResponse?> AccountLogin(LoginRequest request);

	/// <summary>
	/// RefreshToken
	/// </summary>
	/// <param name="request"></param>
	/// <returns></returns>
	Task<LoginResponse?> RefreshToken(TokenRequest request);
}
