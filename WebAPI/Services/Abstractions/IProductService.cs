﻿using Models.Common;
using Models.Entities;
using Models.Filter;
using WebApi.Services.Abstractions;

namespace WebApi.Services.Abstractions;

/// <summary>
/// IProductService
/// </summary>
public interface IProductService : IBaseService<Product>
{
    /// <summary>
    /// FilterDataPaging
    /// </summary>
    /// <param name="filter"></param>
    /// <returns></returns>
    Task<DataPaging<Product>?> FilterDataPaging(ProductFilter filter);

    /// <summary>
    /// GetBySlug
    /// </summary>
    /// <param name="slug"></param>
    /// <returns></returns>
    Task<Product?> GetBySlug(string slug);
}
