﻿using Models.Entities;

namespace WebApi.Services.Abstractions;

public interface INewsCategoryService : IBaseService<NewsCategory>
{
    /// <summary>
    /// GetActiveCategories
    /// </summary>
    /// <returns></returns>
    Task<IEnumerable<NewsCategory>?> GetActiveParentCategories();
}
