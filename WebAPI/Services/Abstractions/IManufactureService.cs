﻿using Models.Common;
using Models.Entities;
using Models.Filter;
using Models.ViewModels;
using WebApi.Services.Abstractions;

namespace WebApi.Services.Abstractions
{
    /// <summary>
    /// IManufactureService
    /// </summary>
    public interface IManufactureService : IBaseService<Manufacture>
    {
        /// <summary>
        /// FilterDataPaging
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        Task<DataPaging<Manufacture>?> FilterDataPaging(ManufactureFilter filter);

        /// <summary>
        /// GetActiveManufactures
        /// </summary>
        /// <returns></returns>
        Task<IEnumerable<Manufacture>?> GetActiveManufactures();

        /// <summary>
        /// GetManufactureBySlug
        /// </summary>
        /// <param name="slug"></param>
        /// <returns></returns>
        Task<ManufactureViewModel?> GetManufactureBySlug(string slug);
    }
}
