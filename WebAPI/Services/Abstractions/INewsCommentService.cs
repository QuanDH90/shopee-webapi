﻿using Models.Entities;

namespace WebApi.Services.Abstractions;

public interface INewsCommentService : IBaseService<NewsComment>
{
}
