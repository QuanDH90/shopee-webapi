﻿using WebApi.Services.Abstractions;
using WebApi.Services.Implementations;

namespace WebApi.Services;

public static class ServicesRegister
{
    public static void RegisterServices(this IServiceCollection services)
    {
        services.AddTransient<IAccountService, AccountService>();
        services.AddTransient<ICategoryService, CategoryService>();
        services.AddTransient<IManufactureService, ManufactureService>();
        services.AddTransient<INewsCategoryService, NewsCategoryService>();
        services.AddTransient<INewsCommentService, NewsCommentService>();
        services.AddTransient<INewsService, NewsService>();
        services.AddTransient<IProductDetailService, ProductDetailService>();
        services.AddTransient<IProductImageService, ProductImageService>();
        services.AddTransient<IProductPropertyService, ProductPropertyService>();
        services.AddTransient<IProductService, ProductService>();
    }
}
