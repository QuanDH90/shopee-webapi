﻿using Models.Common;
using Models.Entities;
using Models.Filter;
using WebApi.Services.Abstractions;
using WebApi.Infrastructure.Abstractions;
using Common.Abstractions;

namespace WebApi.Services.Implementations;

public class ProductImageService : IProductImageService
{
    private readonly IProductImageRepository _productImageRepository;

    private readonly ILogProvider _logProvider;

    public ProductImageService(IProductImageRepository productImageRepository, ILogProvider logProvider)
    {
        _productImageRepository = productImageRepository;
        _logProvider = logProvider;
    }

    /// <inheritdoc/>
    public async Task<string> CreateAsync(ProductImage entity)
    {
        try
        {
            var res = await _productImageRepository.CreateAsync(entity);
            return res;
        }
        catch (Exception ex)
        {
            _logProvider.Error(ex);
            throw new Exception(ex.Message);
        }
    }

    /// <inheritdoc/>
    public async Task<bool> DeleteAsync(string id)
    {
        try
        {
            var res = await _productImageRepository.DeleteAsync(id);
            return res;
        }
        catch (Exception ex)
        {
            _logProvider.Error(ex);
            throw new Exception(ex.Message);
        }
    }

    /// <inheritdoc/>
    public async Task<DataPaging<ProductImage>?> FilterDataPaging(ProductImageFilter filter)
    {
        try
        {
            var res = await _productImageRepository.FilterDataPaging(filter);
            return res;
        }
        catch (Exception ex)
        {
            _logProvider.Error(ex);
            throw new Exception(ex.Message);
        }
    }

    /// <inheritdoc/>
    public Task<ProductImage?> FindProductImageValue(ProductImageFilter filter)
    {
        try
        {
            var res = _productImageRepository.FindProductImageValue(filter);
            return res;
        }
        catch (Exception ex)
        {
            _logProvider.Error(ex);
            throw new Exception(ex.Message);
        }
    }

    /// <inheritdoc/>
    public async Task<IEnumerable<ProductImage>?> GetAllAsync()
    {
        try
        {
            var res = await _productImageRepository.GetAllAsync();
            return res;
        }
        catch (Exception ex)
        {
            _logProvider.Error(ex);
            throw new Exception(ex.Message);
        }
    }

    /// <inheritdoc/>
    public async Task<ProductImage?> GetAsync(string id)
    {
        try
        {
            var res = await _productImageRepository.GetAsync(id);
            return res;
        }
        catch (Exception ex)
        {
            _logProvider.Error(ex);
            throw new Exception(ex.Message);
        }
    }

    /// <inheritdoc/>
    public async Task<bool> UpdateAsync(ProductImage entity)
    {
        try
        {
            var res = await _productImageRepository.UpdateAsync(entity);
            return res;
        }
        catch (Exception ex)
        {
            _logProvider.Error(ex);
            throw new Exception(ex.Message);
        }
    }

    /// <inheritdoc/>
    public async Task<bool> UpdateProductImageValue(ProductImage entity)
    {
        try
        {
            var find = await GetAsync(entity.Id);

            if (find != null)
            {
                find.Image = entity.Image;
                find.UpdatedAt = DateTime.Now;
                return await _productImageRepository.UpdateAsync(find);
            }
            else
            {
                entity.CreatedAt = DateTime.Now;
                var res = await _productImageRepository.CreateAsync(entity);

                if (!string.IsNullOrEmpty(res))
                {
                    return true;
                }
            }
        }
        catch (Exception ex)
        {
            _logProvider.Error(ex);
            throw new Exception(ex.Message);
        }

        return false;
    }
}
