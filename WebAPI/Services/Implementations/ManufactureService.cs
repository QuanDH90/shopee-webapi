﻿using WebApi.Infrastructure.Abstractions;
using Models.Common;
using Models.Entities;
using Models.Filter;
using WebApi.Services.Abstractions;
using Models.ViewModels;
using Common.Abstractions;

namespace WebApi.Services.Implementations;

/// <inheritdoc/>
internal class ManufactureService : IManufactureService
{
    private readonly IManufactureRepository _manufactureRepository;

    private readonly ILogProvider _logProvider;

    public ManufactureService(IManufactureRepository manufactureRepository, ILogProvider logProvider)
    {
        _manufactureRepository = manufactureRepository;
        _logProvider = logProvider;
    }

    /// <inheritdoc/>
    public async Task<string> CreateAsync(Manufacture entity)
    {
        try
        {
            var createResult = await _manufactureRepository.CreateAsync(entity);
            return createResult;
        }
        catch (Exception ex)
        {
            _logProvider.Error(ex);
            throw new Exception(ex.Message);
        }
    }

    /// <inheritdoc/>
    public async Task<bool> DeleteAsync(string id)
    {
        try
        {
            var deleteResult = await _manufactureRepository.DeleteAsync(id);
            return deleteResult;
        }
        catch (Exception ex)
        {
            _logProvider.Error(ex);
            throw new Exception(ex.Message);
        }
    }

    /// <inheritdoc/>
    public async Task<IEnumerable<Manufacture>?> GetAllAsync()
    {
        try
        {
            var getAllResult = await _manufactureRepository.GetAllAsync();
            return getAllResult;
        }
        catch (Exception ex)
        {
            _logProvider.Error(ex);
            throw new Exception(ex.Message);
        }
    }

    /// <inheritdoc/>
    public async Task<Manufacture?> GetAsync(string id)
    {
        try
        {
            var getOneResult = await _manufactureRepository.GetAsync(id);
            return getOneResult;
        }
        catch (Exception ex)
        {
            _logProvider.Error(ex);
            throw new Exception(ex.Message);
        }
    }

    public async Task<DataPaging<Manufacture>?> FilterDataPaging(ManufactureFilter filter)
    {
        try
        {
            var result = await _manufactureRepository.FilterDataPaging(filter);
            return result;
        }
        catch (Exception ex)
        {
            _logProvider.Error(ex);
            throw new Exception(ex.Message);
        }
    }

    /// <inheritdoc/>
    public async Task<bool> UpdateAsync(Manufacture entity)
    {
        try
        {
            var updateResult = await _manufactureRepository.UpdateAsync(entity);
            return updateResult;
        }
        catch (Exception ex)
        {
            _logProvider.Error(ex);
            throw new Exception(ex.Message);
        }
    }

    /// <inheritdoc/>
    public async Task<IEnumerable<Manufacture>?> GetActiveManufactures()
    {
        try
        {
            var result = await _manufactureRepository.GetActiveManufactures();
            return result;
        }
        catch (Exception ex)
        {
            _logProvider.Error(ex);
            throw new Exception(ex.Message);
        }
    }

    /// <inheritdoc/>
    public async Task<ManufactureViewModel?> GetManufactureBySlug(string slug)
    {
        try
        {
            var manufacture = await _manufactureRepository.GetManufactureBySlug(slug);
            return manufacture;
        }
        catch (Exception ex)
        {
            _logProvider.Error(ex);
            throw new Exception(ex.Message);
        }
    }
}
