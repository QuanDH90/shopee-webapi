﻿using Common.Abstractions;
using Microsoft.IdentityModel.Tokens;
using Models.Cache;
using Models.Entities;
using Models.Jwt.Enums;
using Models.Request;
using Models.Response;
using WebApi.Services.Abstractions;
using WebApi.Infrastructure.Abstractions;
using WebApi.Infrastructure.Implementations;

namespace WebApi.Services.Implementations;

/// <inheritdoc/>
public class AccountService : IAccountService
{
    /// <summary>
    /// IAccountRepository
    /// </summary>
    private readonly IAccountRepository _accountRepository;

    /// <summary>
    /// IJwtProvider
    /// </summary>
    private readonly IJwtProvider _jwtProvider;

    /// <summary>
    /// IMemCacheProvider
    /// </summary>
    private readonly IMemCacheProvider _memCacheProvider;

    /// <summary>
    /// ILogProvider
    /// </summary>
    private readonly ILogProvider _logProvider;

    /// <summary>
    /// IHashProvider
    /// </summary>
    private readonly IHashProvider _hashProvider;

    private readonly string Audiencer;

    private readonly string Issuer;

    private readonly string SessionId;

    public AccountService(IAccountRepository accountRepository, IJwtProvider jwtProvider, IMemCacheProvider memCacheProvider, ILogProvider logProvider, IHashProvider hashProvider)
    {
        _accountRepository = accountRepository;
        _jwtProvider = jwtProvider;
        _memCacheProvider = memCacheProvider;
        _logProvider = logProvider;
        Audiencer = "salvation-client";
        Issuer = "salvation-auth-service";
        SessionId = Guid.NewGuid().ToString();
        _hashProvider = hashProvider;
    }

    /// <inheritdoc/>
    public async Task<string> CreateAsync(Account entity)
    {
        try
        {
            var res = await _accountRepository.CreateAsync(entity);
            return res;
        }
        catch (Exception ex)
        {
            _logProvider.Error(ex);
            throw new Exception(ex.Message);
        }
    }

    /// <inheritdoc/>
    public async Task<bool> DeleteAsync(string id)
    {
        try
        {
            var res = await _accountRepository.DeleteAsync(id);
            return res;
        }
        catch (Exception ex)
        {
            _logProvider.Error(ex);
            throw new Exception(ex.Message);
        }
    }

    /// <inheritdoc/>
    public async Task<IEnumerable<Account>?> GetAllAsync()
    {
        try
        {
            var res = await _accountRepository.GetAllAsync();
            return res;
        }
        catch (Exception ex)
        {
            _logProvider.Error(ex);
            throw new Exception(ex.Message);
        }
    }

    /// <inheritdoc/>
    public async Task<Account?> GetAsync(string id)
    {
        try
        {
            var res = await _accountRepository.GetAsync(id);
            return res;
        }
        catch (Exception ex)
        {
            _logProvider.Error(ex);
            throw new Exception(ex.Message);
        }
    }

    /// <inheritdoc/>
    public async Task<LoginResponse?> AccountLogin(LoginRequest request)
    {
        try
        {
            var res = await _accountRepository.GetOneByEmail(request.Email);
            var hashPassword = _hashProvider.CreateMD5(request.Password);

            if (res!.Password.Equals(hashPassword))
            {
                if (res != null && res?.Type == request.Type)
                {
                    Tuple<string, int>? refreshToken = null;
                    var accessTokenExpiredMinutes = 12 * 60;

                    if (request.RememberMe)
                    {
                        refreshToken = _jwtProvider.GenerateJwt(res, Audiencer, Issuer, SessionId, JwtType.RefreshToken);
                        accessTokenExpiredMinutes = 10;
                    }

                    var accessToken = _jwtProvider.GenerateJwt(res, Audiencer, Issuer, SessionId, JwtType.AccessToken, accessTokenExpiredMinutes);
                    var accountCache = new AccountCache
                    {
                        AccountData = res,
                        RefreshToken = refreshToken!.Item1
                    };

                    _memCacheProvider.Set(res.Id, accountCache);

                    var result = new LoginResponse
                    {
                        AccessToken = accessToken.Item1,
                        AccessTokenExpiredAfter = accessToken.Item2,
                        RefreshToken = refreshToken.Item1,
                        RefreshTokenExpiredAfter = refreshToken.Item2,
                        Avatar = res.Avatar!,
                        Email = res.Email,
                        Fullname = res.Fullname
                    };

                    return result;
                }
            }
        }
        catch (Exception ex)
        {
            _logProvider.Error(ex);
            throw new Exception(ex.Message);
        }

        return null;
    }

    /// <inheritdoc/>
    public async Task<bool> UpdateAsync(Account entity)
    {
        try
        {
            var res = await _accountRepository.UpdateAsync(entity);
            return res;
        }
        catch (Exception ex)
        {
            _logProvider.Error(ex);
            throw new Exception(ex.Message);
        }
    }

    /// <inheritdoc/>
    public async Task<LoginResponse?> RefreshToken(TokenRequest request)
    {
        try
        {
            var accountCache = (AccountCache)_memCacheProvider.Get(request.Id);
            var tokenCache = accountCache.RefreshToken;

            if (!string.IsNullOrEmpty(tokenCache))
            {
                if (_jwtProvider.ValidateJwt(tokenCache, JwtType.RefreshToken))
                {
                    var account = await GetAsync(request.Id);

                    if (account != null && account.Equals(accountCache.AccountData))
                    {
                        var accessToken = _jwtProvider.GenerateJwt(account, Audiencer, Issuer, SessionId, JwtType.AccessToken);

                        var result = new LoginResponse
                        {
                            AccessToken = accessToken.Item1,
                            AccessTokenExpiredAfter = accessToken.Item2,
                            Avatar = account.Avatar!,
                            Email = account.Email,
                            Fullname = account.Fullname
                        };

                        return result;
                    }
                }
            }
        }
        catch (Exception ex)
        {
            _logProvider.Error(ex);
            throw new Exception(ex.Message);
        }

        return null;
    }
}
