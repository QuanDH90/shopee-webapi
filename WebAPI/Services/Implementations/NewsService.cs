﻿using Common.Abstractions;
using Models.Common;
using Models.Entities;
using Models.Filter;
using WebApi.Infrastructure.Abstractions;
using WebApi.Services.Abstractions;

namespace WebApi.Services.Implementations;

public class NewsService : INewsService
{
    private readonly INewsRepository _newsRepository;

    private readonly INewsCategoryRepository _newsCategoryRepository;

    private readonly ILogProvider _logProvider;

    public NewsService(INewsRepository newsRepository, ILogProvider logProvider, INewsCategoryRepository newsCategoryRepository)
    {
        _newsRepository = newsRepository;
        _logProvider = logProvider;
        _newsCategoryRepository = newsCategoryRepository;
    }

    public async Task<string> CreateAsync(News entity)
    {
        try
        {
            var result = await _newsRepository.CreateAsync(entity);
            return result;
        }
        catch (Exception ex)
        {
            _logProvider.Error(ex);
            throw new Exception(ex.Message);
        }
    }

    public async Task<bool> CreateOrUpdateAsync(News entity)
    {
        try
        {
            var categoryFind = await _newsCategoryRepository.GetAsync(entity.CategoryId);
            if (categoryFind == null)
            {
                throw new Exception("Category not found");
            }

            entity.CategoryName = categoryFind.Name;
            var find = await _newsRepository.GetAsync(entity.Id);

            if (find == null)
            {
                entity.CreatedAt = DateTime.Now;
                var result = await _newsRepository.CreateAsync(entity);
                return !string.IsNullOrEmpty(result);
            }
            else
            {
                find.UpdatedAt = DateTime.Now;
                var result = await _newsRepository.UpdateAsync(entity);
                return result;
            }
        }
        catch (Exception ex)
        {
            _logProvider.Error(ex);
            throw new Exception(ex.Message);
        }
    }

    public async Task<bool> DeleteAsync(string id)
    {
        try
        {
            var result = await _newsRepository.DeleteAsync(id);
            return result;
        }
        catch (Exception ex)
        {
            _logProvider.Error(ex);
            throw new Exception(ex.Message);
        }
    }

    public Task<DataPaging<News>?> FilterDataPagingAsync(NewFilter filter)
    {
        try
        {
            var result = _newsRepository.FilterDataPagingAsync(filter);
            return result;
        }
        catch (Exception ex)
        {
            _logProvider.Error(ex);
            throw new Exception(ex.Message);
        }
    }

    public async Task<IEnumerable<News>?> GetAllAsync()
    {
        try
        {
            var result = await _newsRepository.GetAllAsync();
            return result;
        }
        catch (Exception ex)
        {
            _logProvider.Error(ex);
            throw new Exception(ex.Message);
        }
    }

    public async Task<News?> GetAsync(string id)
    {
        try
        {
            var result = await _newsRepository.GetAsync(id);
            return result;
        }
        catch (Exception ex)
        {
            _logProvider.Error(ex);
            throw new Exception(ex.Message);
        }
    }

    public async Task<bool> UpdateAsync(News entity)
    {
        try
        {
            var result = await _newsRepository.UpdateAsync(entity);
            return result;
        }
        catch (Exception ex)
        {
            _logProvider.Error(ex);
            throw new Exception(ex.Message);
        }
    }
}
