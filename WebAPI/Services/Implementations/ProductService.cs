﻿using Common.Abstractions;
using WebApi.Infrastructure.Abstractions;
using Models.Common;
using Models.Entities;
using Models.Filter;
using WebApi.Services.Abstractions;

namespace WebApi.Services.Implementations;

/// <inheritdoc/>
internal class ProductService : IProductService
{
    private readonly ICategoryService _categoryService;

    private readonly IManufactureService _manufactureService;

    private readonly IProductRepository _productRepository;

    private readonly ILogProvider _logProvider;

    public ProductService(ICategoryService categoryService, IManufactureService manufactureService, IProductRepository productRepository, ILogProvider logProvider)
    {
        _categoryService = categoryService;
        _manufactureService = manufactureService;
        _productRepository = productRepository;
        _logProvider = logProvider;
    }

    /// <inheritdoc/>
    public async Task<string> CreateAsync(Product entity)
    {
        try
        {
            var categoryFind = await _categoryService.GetAsync(entity.CategoryId);
            if (categoryFind != null)
            {
                entity.CategoryName = categoryFind.Name;
            }

            var manufactureFind = await _manufactureService.GetAsync(entity.ManufactureId);
            if (manufactureFind != null)
            {
                entity.ManufactureName = manufactureFind.Name;
            }

            var createResult = await _productRepository.CreateAsync(entity);
            return createResult;
        }
        catch (Exception ex)
        {
            _logProvider.Error(ex);
            throw new Exception(ex.Message);
        }
    }

    /// <inheritdoc/>
    public async Task<bool> DeleteAsync(string id)
    {
        try
        {
            var deleteResult = await _productRepository.DeleteAsync(id);
            return deleteResult;
        }
        catch (Exception ex)
        {
            _logProvider.Error(ex);
            throw new Exception(ex.Message);
        }
    }

    /// <inheritdoc/>
    public async Task<DataPaging<Product>?> FilterDataPaging(ProductFilter filter)
    {
        try
        {
            if (!string.IsNullOrEmpty(filter.CategoryId))
            {
                // step 0: check level of category
                var categoryFind = await _categoryService.GetAsync(filter.CategoryId);
                if (categoryFind != null)
                {
                    var categoryIdList = new List<string>();
                    switch (categoryFind.Level)
                    {
                        case 0:
                            // get children of level 1
                            var levelOneCategories = await _categoryService.GetChildrenCategoryByParentId(categoryFind.Id);
                            if (levelOneCategories != null && levelOneCategories.Any())
                            {
                                foreach (var category in levelOneCategories)
                                {
                                    categoryIdList.Add(category.Id);
                                    // get children of level 2
                                    var levelTwoCategories = await _categoryService.GetChildrenCategoryByParentId(category.Id);
                                    if (levelTwoCategories != null && levelTwoCategories.Any())
                                    {
                                        foreach (var categoryTwo in levelTwoCategories)
                                        {
                                            categoryIdList.Add(categoryTwo.Id);
                                        }
                                    }
                                }
                            }
                            break;
                        case 1:
                            // get children of level 2
                            var levelTwoOfOneCategories = await _categoryService.GetChildrenCategoryByParentId(categoryFind.Id);
                            if (levelTwoOfOneCategories != null && levelTwoOfOneCategories.Any())
                            {
                                foreach (var category in levelTwoOfOneCategories)
                                {
                                    categoryIdList.Add(category.Id);
                                }
                            }
                            break;
                        default:
                            break;
                    }
                    categoryIdList.Add(categoryFind.Id);
                    filter.CategoryIdList = categoryIdList;
                }
            }

            var res = await _productRepository.FilterDataPaging(filter);
            return res;
        }
        catch (Exception ex)
        {
            _logProvider.Error(ex);
            throw new Exception(ex.Message);
        }
    }

    /// <inheritdoc/>
    public async Task<IEnumerable<Product>?> GetAllAsync()
    {
        try
        {
            var getAllResult = await _productRepository.GetAllAsync();
            return getAllResult;
        }
        catch (Exception ex)
        {
            _logProvider.Error(ex);
            throw new Exception(ex.Message);
        }
    }

    /// <inheritdoc/>
    public async Task<Product?> GetAsync(string id)
    {
        try
        {
            var getOneResult = await _productRepository.GetAsync(id);
            return getOneResult;
        }
        catch (Exception ex)
        {
            _logProvider.Error(ex);
            throw new Exception(ex.Message);
        }
    }

    /// <inheritdoc/>
    public async Task<Product?> GetBySlug(string slug)
    {
        try
        {
            var result = await _productRepository.GetBySlug(slug);
            return result;
        }
        catch (Exception ex)
        {
            _logProvider.Error(ex);
            throw new Exception(ex.Message);
        }
    }

    /// <inheritdoc/>
    public async Task<bool> UpdateAsync(Product entity)
    {
        try
        {
            var categoryFind = await _categoryService.GetAsync(entity.CategoryId);
            if (categoryFind != null)
            {
                entity.CategoryName = categoryFind.Name;
            }

            var manufactureFind = await _manufactureService.GetAsync(entity.ManufactureId);
            if (manufactureFind != null)
            {
                entity.ManufactureName = manufactureFind.Name;
            }

            var updateResult = await _productRepository.UpdateAsync(entity);
            return updateResult;
        }
        catch (Exception ex)
        {
            _logProvider.Error(ex);
            throw new Exception(ex.Message);
        }
    }
}
