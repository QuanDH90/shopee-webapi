﻿using Common.Abstractions;
using Models.Entities;
using WebApi.Infrastructure.Abstractions;
using WebApi.Services.Abstractions;

namespace WebApi.Services.Implementations;

public class NewsCommentService : INewsCommentService
{
    private readonly INewsCommentRepository _newsCommentRepository;

    private readonly ILogProvider _logProvider;

    public NewsCommentService(INewsCommentRepository newsCommentRepository, ILogProvider logProvider)
    {
        _newsCommentRepository = newsCommentRepository;
        _logProvider = logProvider;
    }

    public async Task<string> CreateAsync(NewsComment entity)
    {
        try
        {
            var result = await _newsCommentRepository.CreateAsync(entity);
            return result;
        }
        catch (Exception ex)
        {
            _logProvider.Error(ex);
            throw new Exception(ex.Message);
        }
    }

    public async Task<bool> DeleteAsync(string id)
    {
        try
        {
            var result = await _newsCommentRepository.DeleteAsync(id);
            return result;
        }
        catch (Exception ex)
        {
            _logProvider.Error(ex);
            throw new Exception(ex.Message);
        }
    }

    public async Task<IEnumerable<NewsComment>?> GetAllAsync()
    {
        try
        {
            var result = await _newsCommentRepository.GetAllAsync();
            return result;
        }
        catch (Exception ex)
        {
            _logProvider.Error(ex);
            throw new Exception(ex.Message);
        }
    }

    public async Task<NewsComment?> GetAsync(string id)
    {
        try
        {
            var result = await _newsCommentRepository.GetAsync(id);
            return result;
        }
        catch (Exception ex)
        {
            _logProvider.Error(ex);
            throw new Exception(ex.Message);
        }
    }

    public async Task<bool> UpdateAsync(NewsComment entity)
    {
        try
        {
            var result = await _newsCommentRepository.UpdateAsync(entity);
            return result;
        }
        catch (Exception ex)
        {
            _logProvider.Error(ex);
            throw new Exception(ex.Message);
        }
    }
}
