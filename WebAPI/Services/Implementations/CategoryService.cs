﻿using WebApi.Infrastructure.Abstractions;
using Models.Common;
using Models.Entities;
using Models.Filter;
using Models.ViewModels;
using WebApi.Services.Abstractions;
using Common.Abstractions;

namespace WebApi.Services.Implementations;

/// <inheritdoc/>
internal class CategoryService : ICategoryService
{
    private readonly ICategoryRepository _categoryRepository;

    private readonly ILogProvider _logProvider;

    public CategoryService(ICategoryRepository categoryRepository, ILogProvider logProvider)
    {
        _categoryRepository = categoryRepository;
        _logProvider = logProvider;
    }

    /// <inheritdoc/>
    public async Task<string> CreateAsync(Category entity)
    {
        try
        {
            var createResult = await _categoryRepository.CreateAsync(entity);
            return createResult;
        }
        catch (Exception ex)
        {
            _logProvider.Error(ex);
            throw new Exception(ex.Message);
        }
    }

    /// <inheritdoc/>
    public async Task<bool> DeleteAsync(string id)
    {
        try
        {
            var deleteResult = await _categoryRepository.DeleteAsync(id);
            return deleteResult;
        }
        catch (Exception ex)
        {
            _logProvider.Error(ex);
            throw new Exception(ex.Message);
        }
    }

    /// <inheritdoc/>
    public async Task<IEnumerable<Category>?> GetAllAsync()
    {
        try
        {
            var getAllResult = await _categoryRepository.GetAllAsync();
            return getAllResult;
        }
        catch (Exception ex)
        {
            _logProvider.Error(ex);
            throw new Exception(ex.Message);
        }
    }

    /// <inheritdoc/>
    public async Task<Category?> GetAsync(string id)
    {
        try
        {
            var getOneResult = await _categoryRepository.GetAsync(id);
            return getOneResult;
        }
        catch (Exception ex)
        {
            _logProvider.Error(ex);
            throw new Exception(ex.Message);
        }
    }

    /// <inheritdoc/>
    public async Task<bool> UpdateAsync(Category entity)
    {
        try
        {
            var updateResult = await _categoryRepository.UpdateAsync(entity);
            return updateResult;
        }
        catch (Exception ex)
        {
            _logProvider.Error(ex);
            throw new Exception(ex.Message);
        }
    }

    public async Task<IEnumerable<CategoryViewModel>?> GetShowingCategories()
    {
        try
        {
            var result = new List<CategoryViewModel>();
            var categories = await _categoryRepository.GetActiveParentCategories();

            if (categories != null && categories.Any())
            {
                foreach (var category in categories)
                {
                    var categoryViewModel = new CategoryViewModel
                    {
                        Id = category.Id,
                        Name = category.Name,
                        Image = category.Image,
                        ParentId = category.ParentId,
                        Slug = category.Slug
                    };

                    var children = await _categoryRepository.GetActiveChildrenCategoriesByParentId(category.Id);

                    if (children != null && children.Any())
                    {
                        foreach (var child in children)
                        {
                            var childViewModel = new CategoryViewModel
                            {
                                Id = child.Id,
                                Name = child.Name,
                                Image = child.Image,
                                ParentId = child.ParentId,
                                Slug = child.Slug
                            };

                            result.Add(childViewModel);
                        }
                    }

                    result.Add(categoryViewModel);
                }
            }

            return result;
        }
        catch (Exception ex)
        {
            _logProvider.Error(ex);
            throw new Exception(ex.Message);
        }
    }

    /// <inheritdoc/>
    public async Task<IEnumerable<CategoryViewModel>?> GetActiveCategories()
    {
        try
        {
            var result = new List<CategoryViewModel>();
            var categories = await _categoryRepository.GetActiveParentCategories();

            if (categories != null && categories.Any())
            {
                foreach (var category in categories)
                {
                    var categoryViewModel = new CategoryViewModel
                    {
                        Id = category.Id,
                        Name = category.Name,
                        Image = category.Image,
                        ParentId = category.ParentId,
                        Slug = category.Slug
                    };

                    var children = await _categoryRepository.GetActiveChildrenCategoriesByParentId(category.Id);

                    if (children != null && children.Any())
                    {
                        var childrenViewModel = new List<CategoryViewModel>();

                        foreach (var child in children)
                        {
                            var childViewModel = new CategoryViewModel
                            {
                                Id = child.Id,
                                Name = child.Name,
                                Image = child.Image,
                                ParentId = child.ParentId,
                                Slug = child.Slug
                            };

                            var subChildren = await _categoryRepository.GetActiveChildrenCategoriesByParentId(childViewModel.Id);

                            if (subChildren != null && subChildren.Any())
                            {
                                var subChildViewModelList = new List<CategoryViewModel>();

                                foreach (var subChild in subChildren)
                                {
                                    var subChildViewModel = new CategoryViewModel
                                    {
                                        Id = subChild.Id,
                                        Name = subChild.Name,
                                        Image = subChild.Image,
                                        ParentId = subChild.ParentId,
                                        Slug = subChild.Slug
                                    };

                                    subChildViewModelList.Add(subChildViewModel);
                                }

                                childViewModel.Children = subChildViewModelList;
                            }

                            childrenViewModel.Add(childViewModel);
                        }

                        categoryViewModel.Children = childrenViewModel;
                    }

                    result.Add(categoryViewModel);
                }
            }

            return result;
        }
        catch (Exception ex)
        {
            _logProvider.Error(ex);
            throw new Exception(ex.Message);
        }
    }

    /// <inheritdoc/>
    public async Task<DataPaging<Category>?> FilterDataPaging(CategoryFilter filter)
    {
        try
        {
            var result = await _categoryRepository.FilterDataPaging(filter);
            return result;
        }
        catch (Exception ex)
        {
            _logProvider.Error(ex);
            throw new Exception(ex.Message);
        }
    }

    /// <inheritdoc/>
    public async Task<IEnumerable<CategoryViewModel>?> GetParentActiveCategories()
    {
        try
        {
            var result = new List<CategoryViewModel>();
            var categories = await _categoryRepository.GetActiveParentCategories();

            if (categories != null && categories.Count() > 0)
            {
                foreach (var category in categories)
                {
                    var viewModel = new CategoryViewModel
                    {
                        Id = category.Id,
                        Name = category.Name,
                        Image = category.Image,
                        Slug = category.Slug
                    };

                    result.Add(viewModel);
                }
            }

            return result;
        }
        catch (Exception ex)
        {
            _logProvider.Error(ex);
            throw new Exception(ex.Message);
        }
    }

    /// <inheritdoc/>
    public async Task<CategoryViewModel?> GetCategoryTree(string id)
    {
        try
        {
            var category = await GetAsync(id);

            if (category != null)
            {
                var categoryViewModel = new CategoryViewModel
                {
                    Id = category.Id,
                    Name = category.Name,
                    Image = category.Image,
                    Slug = category.Slug,
                    ParentId = category.ParentId
                };

                var children = await _categoryRepository.GetActiveChildrenCategoriesByParentId(category.Id);

                if (children != null && children.Count() > 0)
                {
                    var childrenViewModel = new List<CategoryViewModel>();

                    foreach (var child in children)
                    {
                        var viewModel = new CategoryViewModel
                        {
                            Id = child.Id,
                            Name = child.Name,
                            Image = child.Image,
                            Slug = child.Slug,
                            ParentId = child.ParentId
                        };

                        childrenViewModel.Add(viewModel);
                    }

                    categoryViewModel.Children = childrenViewModel;
                }

                return categoryViewModel;
            }

            return null;
        }
        catch (Exception ex)
        {
            _logProvider.Error(ex);
            throw new Exception(ex.Message);
        }
    }

    /// <inheritdoc/>
    public async Task<CategoryViewModel?> GetCategoryTreeBySlug(string slug)
    {
        try
        {
            var category = await _categoryRepository.GetCategoryTreeBySlug(slug);
            return category;
        }
        catch (Exception ex)
        {
            _logProvider.Error(ex);
            throw new Exception(ex.Message);
        }
    }

    /// <inheritdoc/>
    public async Task<IEnumerable<Category>?> GetChildrenCategoryByParentId(string parentId)
    {
        try
        {
            var children = await _categoryRepository.GetActiveChildrenCategoriesByParentId(parentId);
            return children;
        }
        catch (Exception ex)
        {
            _logProvider.Error(ex);
            throw new Exception(ex.Message);
        }
    }
}
