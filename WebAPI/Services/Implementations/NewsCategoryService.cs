﻿using Common.Abstractions;
using Models.Entities;
using WebApi.Infrastructure.Abstractions;
using WebApi.Services.Abstractions;

namespace WebApi.Services.Implementations;

public class NewsCategoryService : INewsCategoryService
{
    private readonly INewsCategoryRepository _newsCategoryRepository;

    private readonly ILogProvider _logProvider;

    public NewsCategoryService(INewsCategoryRepository newsCategoryRepository, ILogProvider logProvider)
    {
        _newsCategoryRepository = newsCategoryRepository;
        _logProvider = logProvider;
    }

    public async Task<string> CreateAsync(NewsCategory entity)
    {
        try
        {
            var result = await _newsCategoryRepository.CreateAsync(entity);
            return result;
        }
        catch (Exception ex)
        {
            _logProvider.Error(ex);
            throw new Exception(ex.Message);
        }
    }

    public async Task<bool> DeleteAsync(string id)
    {
        try
        {
            var result = await _newsCategoryRepository.DeleteAsync(id);
            return result;
        }
        catch (Exception ex)
        {
            _logProvider.Error(ex);
            throw new Exception(ex.Message);
        }
    }

    public async Task<IEnumerable<NewsCategory>?> GetActiveParentCategories()
    {
        try
        {
            var result = await _newsCategoryRepository.GetActiveParentCategories();
            return result;
        }
        catch (Exception ex)
        {
            _logProvider.Error(ex);
            throw new Exception(ex.Message);
        }
    }

    public async Task<IEnumerable<NewsCategory>?> GetAllAsync()
    {
        try
        {
            var result = await _newsCategoryRepository.GetAllAsync();
            return result;
        }
        catch (Exception ex)
        {
            _logProvider.Error(ex);
            throw new Exception(ex.Message);
        }
    }

    public async Task<NewsCategory?> GetAsync(string id)
    {
        try
        {
            var result = await _newsCategoryRepository.GetAsync(id);
            return result;
        }
        catch (Exception ex)
        {
            _logProvider.Error(ex);
            throw new Exception(ex.Message);
        }
    }

    public async Task<bool> UpdateAsync(NewsCategory entity)
    {
        try
        {
            var result = await _newsCategoryRepository.UpdateAsync(entity);
            return result;
        }
        catch (Exception ex)
        {
            _logProvider.Error(ex);
            throw new Exception(ex.Message);
        }
    }
}
