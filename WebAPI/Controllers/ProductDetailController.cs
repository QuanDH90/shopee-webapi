﻿using Common.Abstractions;
using Microsoft.AspNetCore.Mvc;
using Models.Filter;
using Models.Request;
using WebApi.Controllers.Base;
using WebApi.Services.Abstractions;

namespace WebAPI.Controllers;

[Route("api/[controller]")]
public class ProductDetailController : BaseController
{
    private readonly IProductDetailService _productDetailService;

    private readonly ILogProvider _logProvider;

    public ProductDetailController(IProductDetailService productDetailService, ILogProvider logProvider)
    {
        _productDetailService = productDetailService;
        _logProvider = logProvider;
    }

    [HttpGet("find-by-product-id/{productId}")]
    public async Task<IActionResult> FindProductDetailByProductId(string productId)
    {
        try
        {
            var res = await _productDetailService.FilterDataPaging(new ProductDetailFilter { ProductId = productId });
            return Ok(SuccessData(res.Data));
        }
        catch (Exception ex)
        {
            _logProvider.Error(ex);
            return Ok(ErrorMessage(ex.Message));
        }
    }

    [HttpPost("find-by-filter")]
    public async Task<IActionResult> FindProductDetailValue(ProductDetailFilter filter)
    {
        try
        {
            var res = await _productDetailService.FindProductDetailValue(filter);
            return Ok(SuccessData(res));
        }
        catch (Exception ex)
        {
            _logProvider.Error(ex);
            return Ok(ErrorMessage(ex.Message));
        }
    }

    [HttpPost("update-data")]
    public async Task<IActionResult> UpdateProductDetailValue(ProductDetailUpdateRequest request)
    {
        try
        {
            var res = await _productDetailService.UpdateProductDetailValue(request);
            return Ok(SuccessData(res));
        }
        catch (Exception ex)
        {
            _logProvider.Error(ex);
            return Ok(ErrorMessage(ex.Message));
        }
    }
}
