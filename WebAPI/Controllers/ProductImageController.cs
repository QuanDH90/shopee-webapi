﻿using Common.Abstractions;
using Microsoft.AspNetCore.Mvc;
using Models.Entities;
using Models.Filter;
using WebApi.Services.Abstractions;
using WebApi.Controllers.Base;

namespace WebApi.Controllers;

[Route("api/[controller]")]
[ApiController]
public class ProductImageController : BaseController
{
    private readonly IProductImageService _productImageService;

    private readonly ILogProvider _logProvider;

    public ProductImageController(IProductImageService productImageService, ILogProvider logProvider)
    {
        _productImageService = productImageService;
        _logProvider = logProvider;
    }

    [HttpGet("find-by-product-id/{productId}")]
    public async Task<IActionResult> FindProductImageByProductId(string productId)
    {
        try
        {
            var res = await _productImageService.FilterDataPaging(new ProductImageFilter { ProductId = productId });
            return Ok(SuccessData(res.Data));
        }
        catch (Exception ex)
        {
            _logProvider.Error(ex);
            return Ok(ErrorMessage(ex.Message));
        }
    }

    [HttpPost("find-by-filter")]
    public async Task<IActionResult> FindProductImageValue(ProductImageFilter filter)
    {
        try
        {
            var res = await _productImageService.FindProductImageValue(filter);
            return Ok(SuccessData(res));
        }
        catch (Exception ex)
        {
            _logProvider.Error(ex);
            return Ok(ErrorMessage(ex.Message));
        }
    }

    [HttpPost("update-data")]
    public async Task<IActionResult> UpdateProductImageValue(ProductImage entity)
    {
        try
        {
            var res = await _productImageService.UpdateProductImageValue(entity);
            return Ok(SuccessData(res));
        }
        catch (Exception ex)
        {
            _logProvider.Error(ex);
            return Ok(ErrorMessage(ex.Message));
        }
    }

    [HttpGet("delete/{id}")]
    public async Task<IActionResult> DeleteProductImage(string id)
    {
        try
        {
            var res = await _productImageService.DeleteAsync(id);
            return Ok(SuccessData(res));
        }
        catch (Exception ex)
        {
            _logProvider.Error(ex);
            return Ok(ErrorMessage(ex.Message));
        }
    }
}
