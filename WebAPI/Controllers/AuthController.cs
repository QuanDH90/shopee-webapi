﻿using Common.Abstractions;
using Microsoft.AspNetCore.Mvc;
using Models.Request;
using WebApi.Services.Abstractions;
using WebApi.Controllers.Base;

namespace WebApi.Controllers;

[Route("api/[controller]")]
[ApiController]
public class AuthController : BaseController
{
	private readonly ILogProvider _logProvider;

	private readonly IAccountService _accountService;

	public AuthController(ILogProvider logProvider, IAccountService accountService)
	{
		_logProvider = logProvider;
		_accountService = accountService;
	}

	[HttpPost("login")]
	public async Task<IActionResult> AccountLogin(LoginRequest request)
	{
		try
		{
			var res = await _accountService.AccountLogin(request);
			
			if (res != null)
			{
                return Ok(SuccessData(res, "Login successful!"));
            }

			return Ok(ErrorMessage("Login failed"));
		}
		catch (Exception ex)
		{
			_logProvider.Error(ex);
			return BadRequest(ex);
		}
	}

	[HttpPost("refresh-token")]
	public async Task<IActionResult> RefreshToken(TokenRequest request)
	{
		try
		{
			var res = await _accountService.RefreshToken(request);
			return Ok(SuccessData(res));
		}
		catch (Exception ex)
		{
			_logProvider.Error(ex);
			return BadRequest(ex);
		}
	}
}
