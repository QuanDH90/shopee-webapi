﻿using Common.Abstractions;
using Microsoft.AspNetCore.Mvc;
using Models.Entities;
using WebApi.Controllers.Base;
using WebApi.Services.Abstractions;

namespace WebApi.Controllers;

[Route("api/[controller]")]
[ApiController]
public class NewsCategoryController : BaseController
{
    private readonly INewsCategoryService _newsCategoryService;

    private readonly ILogProvider _logProvider;

    public NewsCategoryController(INewsCategoryService newsCategoryService, ILogProvider logProvider)
    {
        _newsCategoryService = newsCategoryService;
        _logProvider = logProvider;
    }

    [HttpPost("create-category")]
    public async Task<IActionResult> CreateAsync(NewsCategory entity)
    {
        try
        {
            var result = await _newsCategoryService.CreateAsync(entity);
            return Ok(SuccessData(result));
        }
        catch (Exception ex)
        {
            _logProvider.Error(ex);
            return BadRequest(ex.Message);
        }
    }

    [HttpGet("delete-category/{id}")]
    public async Task<IActionResult> DeleteAsync(string id)
    {
        try
        {
            var result = await _newsCategoryService.DeleteAsync(id);
            return Ok(SuccessData(result));
        }
        catch (Exception ex)
        {
            _logProvider.Error(ex);
            return BadRequest(ex.Message);
        }
    }

    [HttpGet("active-parent-categories")]
    public async Task<IActionResult> GetActiveParentCategories()
    {
        try
        {
            var result = await _newsCategoryService.GetActiveParentCategories();
            return Ok(SuccessData(result));
        }
        catch (Exception ex)
        {
            _logProvider.Error(ex);
            return BadRequest(ex.Message);
        }
    }

    [HttpGet("get-all-categories")]
    public async Task<IActionResult> GetAllAsync()
    {
        try
        {
            var result = await _newsCategoryService.GetAllAsync();
            return Ok(SuccessData(result));
        }
        catch (Exception ex)
        {
            _logProvider.Error(ex);
            return BadRequest(ex.Message);
        }
    }

    [HttpGet("get-category-by-id/{id}")]
    public async Task<IActionResult> GetAsync(string id)
    {
        try
        {
            var result = await _newsCategoryService.GetAsync(id);
            return Ok(SuccessData(result));
        }
        catch (Exception ex)
        {
            _logProvider.Error(ex);
            return BadRequest(ex.Message);
        }
    }

    [HttpPut("update-category")]
    public async Task<IActionResult> UpdateAsync(NewsCategory entity)
    {
        try
        {
            var result = await _newsCategoryService.UpdateAsync(entity);
            return Ok(SuccessData(result));
        }
        catch (Exception ex)
        {
            _logProvider.Error(ex);
            return BadRequest(ex.Message);
        }
    }
}
