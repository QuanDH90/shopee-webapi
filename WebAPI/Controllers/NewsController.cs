﻿using Common.Abstractions;
using Microsoft.AspNetCore.Mvc;
using Models.Entities;
using Models.Filter;
using WebApi.Controllers.Base;
using WebApi.Services.Abstractions;

namespace WebApi.Controllers;

[Route("api/[controller]")]
[ApiController]
public class NewsController : BaseController
{
    private readonly INewsService _newsService;

    private readonly ILogProvider _logProvider;

    public NewsController(INewsService newsService, ILogProvider logProvider)
    {
        _newsService = newsService;
        _logProvider = logProvider;
    }

    [HttpGet("get-all")]
    public async Task<IActionResult> GetAllAsync()
    {
        try
        {
            var result = await _newsService.GetAllAsync();
            return Ok(SuccessData(result));
        }
        catch (Exception ex)
        {
            _logProvider.Error(ex);
            return BadRequest(ex.Message);
        }
    }

    [HttpGet("get-by-id/{id}")]
    public async Task<IActionResult> GetAsync(string id)
    {
        try
        {
            var result = await _newsService.GetAsync(id);
            return Ok(SuccessData(result));
        }
        catch (Exception ex)
        {
            _logProvider.Error(ex);
            return BadRequest(ex.Message);
        }
    }

    [HttpPost("create")]
    public async Task<IActionResult> CreateAsync(News entity)
    {
        try
        {
            var result = await _newsService.CreateAsync(entity);
            return Ok(SuccessData(result));
        }
        catch (Exception ex)
        {
            _logProvider.Error(ex);
            return BadRequest(ex.Message);
        }
    }

    [HttpPut("update")]
    public async Task<IActionResult> UpdateAsync(News entity)
    {
        try
        {
            var result = await _newsService.UpdateAsync(entity);
            return Ok(SuccessData(result));
        }
        catch (Exception ex)
        {
            _logProvider.Error(ex);
            return BadRequest(ex.Message);
        }
    }

    [HttpDelete("delete/{id}")]
    public async Task<IActionResult> DeleteAsync(string id)
    {
        try
        {
            var result = await _newsService.DeleteAsync(id);
            return Ok(SuccessData(result));
        }
        catch (Exception ex)
        {
            _logProvider.Error(ex);
            return BadRequest(ex.Message);
        }
    }

    [HttpPost("create-or-update")]
    public async Task<IActionResult> CreateOrUpdateAsync(News entity)
    {
        try
        {
            var result = await _newsService.CreateOrUpdateAsync(entity);
            return Ok(SuccessData(result));
        }
        catch (Exception ex)
        {
            _logProvider.Error(ex);
            return BadRequest(ex.Message);
        }
    }

    [HttpPost("filter-data-paging")]
    public async Task<IActionResult> FilterDataPagingAsync(NewFilter filter)
    {
        try
        {
            var result = await _newsService.FilterDataPagingAsync(filter);
            return Ok(SuccessData(result));
        }
        catch (Exception ex)
        {
            _logProvider.Error(ex);
            return BadRequest(ex.Message);
        }
    }
}
