﻿using Common.Abstractions;
using Microsoft.AspNetCore.Mvc;
using Models.Entities;
using WebApi.Controllers.Base;
using WebApi.Services.Abstractions;

namespace WebApi.Controllers;

[Route("api/[controller]")]
[ApiController]
public class NewsCommentController : BaseController
{
    private readonly INewsCommentService _newsCommentService;

    private readonly ILogProvider _logProvider;

    public NewsCommentController(INewsCommentService newsCommentService, ILogProvider logProvider)
    {
        _newsCommentService = newsCommentService;
        _logProvider = logProvider;
    }

    [HttpPost("create-comment")]
    public async Task<IActionResult> CreateAsync(NewsComment entity)
    {
        try
        {
            var result = await _newsCommentService.CreateAsync(entity);
            return Ok(result);
        }
        catch (Exception ex)
        {
            _logProvider.Error(ex);
            return BadRequest(ex.Message);
        }
    }

    [HttpGet("delete-comment/{id}")]
    public async Task<IActionResult> DeleteAsync(string id)
    {
        try
        {
            var result = await _newsCommentService.DeleteAsync(id);
            return Ok(result);
        }
        catch (Exception ex)
        {
            _logProvider.Error(ex);
            return BadRequest(ex.Message);
        }
    }

    [HttpGet("get-by-news-id/{newsId}")]
    public async Task<IActionResult> GetAsync(string newsId)
    {
        try
        {
            var result = await _newsCommentService.GetAsync(newsId);
            return Ok(result);
        }
        catch (Exception ex)
        {
            _logProvider.Error(ex);
            return BadRequest(ex.Message);
        }
    }

    [HttpGet("get-all-comments")]
    public async Task<IActionResult> GetAllAsync()
    {
        try
        {
            var result = await _newsCommentService.GetAllAsync();
            return Ok(result);
        }
        catch (Exception ex)
        {
            _logProvider.Error(ex);
            return BadRequest(ex.Message);
        }
    }

    [HttpPut("update-comment")]
    public async Task<IActionResult> UpdateAsync(NewsComment entity)
    {
        try
        {
            var result = await _newsCommentService.UpdateAsync(entity);
            return Ok(result);
        }
        catch (Exception ex)
        {
            _logProvider.Error(ex);
            return BadRequest(ex.Message);
        }
    }
}
