using Common;
using Microsoft.EntityFrameworkCore;
using WebApi.Infrastructure;
using WebApi.Models.Context;
using WebApi.Services;
using WebApi.Middlewares;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.RegisterProviders();
builder.Services.RegisterInfrastructures();
builder.Services.RegisterServices();

// Add middlewares
//builder.Services.AddTransient<GlobalErrorHandlingMiddleware>();
//builder.Services.AddTransient<JwtMiddleware>();

builder.Services.AddControllers();

// Add Entity Framework DBContext
builder.Services.AddDbContext<ApplicationDbContext>(options =>
    options.UseSqlServer(builder.Configuration.GetConnectionString("MSSQLConnection")));

// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

// Add middlewares
//GlobalErrorHandlingMiddlewareExtension.UseGlobalErrorHandlingMiddleware(app);
//JwtMiddlewareExtensions.UseJwtMiddleware(app);

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
