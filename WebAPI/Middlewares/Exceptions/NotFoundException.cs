﻿namespace WebApi.Middlewares.Exceptions;

/// <summary>
/// NotFoundException
/// </summary>
public class NotFoundException(string message) : Exception(message)
{
}
