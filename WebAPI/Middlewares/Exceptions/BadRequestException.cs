﻿namespace WebApi.Middlewares.Exceptions;

/// <summary>
/// BadRequestException
/// </summary>
/// <remarks>
/// BadRequestException
/// </remarks>
/// <param name="message"></param>
public class BadRequestException(string message) : Exception(message)
{
}
