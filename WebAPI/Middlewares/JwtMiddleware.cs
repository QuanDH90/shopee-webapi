﻿using Common.Abstractions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.Mvc.Controllers;
using Models.Cache;
using Models.Jwt.Enums;
using System.Net;

namespace WebApi.Middlewares;

/// <summary>
/// JwtMiddleware
/// </summary>
/// <remarks>
/// Constructor
/// </remarks>
/// <param name="next"></param>
/// <param name="logger"></param>
public class JwtMiddleware(
	RequestDelegate next,
	IJwtProvider jwtProvider,
	ICookieProvider cookieProvider,
	IMemCacheProvider memCacheProvider)
{
	/// <summary>
	/// RequestDelegate
	/// </summary>
	private readonly RequestDelegate _next = next;

	/// <summary>
	/// IJwtProvider
	/// </summary>
	private readonly IJwtProvider _jwtProvider = jwtProvider;

	/// <summary>
	/// ICookieProvider
	/// </summary>
	private readonly ICookieProvider _cookieProvider = cookieProvider;

	/// <summary>
	/// IMemCacheProvider
	/// </summary>
	private readonly IMemCacheProvider _memCacheProvider = memCacheProvider;

	/// <summary>
	/// InvokeAsync
	/// </summary>
	/// <param name="context"></param>
	/// <returns></returns>
	public async Task InvokeAsync(HttpContext context)
	{
		var endpoint = context.Features.Get<IEndpointFeature>()?.Endpoint;

		// Kiểm tra xem có phải là ActionEndpoint không
		if (endpoint?.Metadata.GetMetadata<ControllerActionDescriptor>() is ControllerActionDescriptor actionDescriptor)
		{
			// Kiểm tra xem action có được gán AllowAnonymousAttribute không
			var allowAnonymousAttribute = actionDescriptor.MethodInfo.GetCustomAttributes(inherit: true)
				.OfType<AllowAnonymousAttribute>()
				.FirstOrDefault();

			if (allowAnonymousAttribute != null)
			{
				context.Request.Headers.TryGetValue("Authorization", out var authorizationToken);
				var tokenString = Convert.ToString(authorizationToken);

				if (string.IsNullOrEmpty(tokenString))
				{
					context.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
					await context.Response.WriteAsync("Unauthorized!");
					return;
				}

				tokenString = tokenString.Replace("Bearer ", "");
				var validatorTokenString = _jwtProvider.ValidateJwt(tokenString, JwtType.AccessToken);

				if (!validatorTokenString)
				{
					context.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
					await context.Response.WriteAsync("Unauthorized!");
					return;
				}
				else
				{
					var payload = _jwtProvider.DecodeJwtToPayload(tokenString, JwtType.AccessToken);

					if (payload != null)
					{
						var userId = payload.AccountId;
						var cache = _memCacheProvider.Get(userId);

						if (cache != null)
						{
							var accountCache = (AccountCache)cache;
							var validatorRefreshToken = _jwtProvider.ValidateJwt(accountCache.RefreshToken, JwtType.RefreshToken);

							if (validatorRefreshToken)
							{
								var audiencer = "salvation-client";
								var issuer = "salvation-auth-service";
								var sessionId = Guid.NewGuid().ToString();
								var newToken = _jwtProvider.GenerateJwt(accountCache.AccountData, audiencer, issuer, sessionId, JwtType.AccessToken);

								if (!string.IsNullOrEmpty(newToken))
								{
									_cookieProvider.Set("AccessToken", newToken, 600);
								}
							}
						}
					}

					context.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
					await context.Response.WriteAsync("Unauthorized!");
					return;
				}
			}
		}

		await _next(context);
	}
}

/// <summary>
/// JwtMiddlewareExtensions
/// </summary>
public static class JwtMiddlewareExtensions
{
	/// <summary>
	/// UseCustomMiddleware
	/// </summary>
	/// <param name="builder"></param>
	/// <returns></returns>
	public static IApplicationBuilder UseJwtMiddleware(this IApplicationBuilder builder)
	{
		return builder.UseMiddleware<JwtMiddleware>();
	}
}
