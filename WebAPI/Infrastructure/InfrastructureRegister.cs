﻿using WebApi.Infrastructure.Abstractions;
using WebApi.Infrastructure.Implementations;

namespace WebApi.Infrastructure;

public static class InfrastructureRegister
{
    public static void RegisterInfrastructures(this IServiceCollection services)
    {
        services.AddTransient<IAccountRepository, AccountRepository>();
        services.AddTransient<ICategoryRepository, CategoryRepository>();
        services.AddTransient<IManufactureRepository, ManufactureRepository>();
        services.AddTransient<INewsCategoryRepository, NewsCategoryRepository>();
        services.AddTransient<INewsCommentRepository, NewsCommentRepository>();
        services.AddTransient<INewsRepository, NewsRepository>();
        services.AddTransient<IProductDetailRepository, ProductDetailRepository>();
        services.AddTransient<IProductImageRepository, ProductImageRepository>();
        services.AddTransient<IProductPropertyRepository, ProductPropertyRepository>();
        services.AddTransient<IProductRepository, ProductRepository>();
    }
}
