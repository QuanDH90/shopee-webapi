﻿using Common.Abstractions;
using Models.Entities;
using Models.Common;
using Models.Filter;
using WebApi.Infrastructure.Abstractions;
using WebApi.Models.Context;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace WebApi.Infrastructure.Implementations;

internal class ProductRepository : RepositoryBase<Product>, IProductRepository
{
    public ProductRepository(ApplicationDbContext context, ILogProvider logProvider, IConfiguration configuration) : base(context, logProvider, configuration)
    {
    }

    /// <inheritdoc/>
    public async Task<DataPaging<Product>?> FilterDataPaging(ProductFilter filter)
    {
        try
        {
            filter.Page ??= 0;
            filter.Limit ??= 50;
            int offset = filter.Page.Value * filter.Limit.Value;
            var res = _context.Set<Product>().Where(x => !string.IsNullOrEmpty(x.Id));

            if (!string.IsNullOrEmpty(filter.Name))
            {
                res = res.Where(x => x.Name.Contains(filter.Name));
            }

            if (filter.CategoryIdList != null && filter.CategoryIdList.Count > 0)
            {
                res = res.Where(x => filter.CategoryIdList.Contains(x.CategoryId));
            }

            if (!string.IsNullOrEmpty(filter.CategoryName))
            {
                res = res.Where(x => x.CategoryName.Contains(filter.CategoryName));
            }

            if (!string.IsNullOrEmpty(filter.ManufactureId))
            {
                res = res.Where(x => x.ManufactureId.Equals(filter.ManufactureId));
            }

            if (!string.IsNullOrEmpty(filter.ManufactureName))
            {
                res = res.Where(x => x.ManufactureName.Contains(filter.ManufactureName));
            }

            if (filter.UpPrice != null)
            {
                res = res.Where(x => x.Price <= filter.UpPrice);
            }

            if (filter.DownPrice != null)
            {
                res = res.Where(x => x.Price >= filter.DownPrice);
            }

            if (filter.IsActived != null)
            {
                res = res.Where((x) => x.IsActived);
            }

            res = res.OrderByDescending(x => x.CreatedAt);
            var count = await res.CountAsync();
            var result = await res.Skip(offset).Take(filter.Limit.Value).ToListAsync();
            return new DataPaging<Product> { Data = result, PaginationCount = count };
        }
        catch (Exception ex)
        {
            _logProvider.Error(ex);
            throw new Exception(ex.Message);
        }
    }

    /// <inheritdoc/>
    public async Task<Product?> GetBySlug(string slug)
    {
        try
        {
            var res = await _context.Set<Product>().FirstOrDefaultAsync(x => x.Slug.Equals(slug));
            return res;
        }
        catch (Exception ex)
        {
            _logProvider.Error(ex);
            throw new Exception(ex.Message);
        }
    }
}
