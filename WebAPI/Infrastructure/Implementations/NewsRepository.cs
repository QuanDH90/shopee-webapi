﻿using Common.Abstractions;
using Microsoft.EntityFrameworkCore;
using Models.Common;
using Models.Entities;
using Models.Filter;
using WebApi.Infrastructure.Abstractions;
using WebApi.Models.Context;

namespace WebApi.Infrastructure.Implementations;

public class NewsRepository : RepositoryBase<News>, INewsRepository
{
    public NewsRepository(ApplicationDbContext context, ILogProvider logProvider, IConfiguration configuration) : base(context, logProvider, configuration)
    {
    }

    public async Task<DataPaging<News>?> FilterDataPagingAsync(NewFilter filter)
    {
        try
        {
            filter.Page ??= 0;
            filter.Limit ??= 50;
            int offset = filter.Page.Value * filter.Limit.Value;
            filter.Offset = offset;
            var res = _context.Set<News>().Where(x => !string.IsNullOrEmpty(x.Id));

            if (!string.IsNullOrEmpty(filter.CategoryId))
            {
                res = res.Where(x => x.CategoryId.Equals(filter.CategoryId));
            }

            if (!string.IsNullOrEmpty(filter.Title))
            {
                res = res.Where(x => x.Title.Contains(filter.Title));
            }

            if (!string.IsNullOrEmpty(filter.Slug))
            {
                res = res.Where(x => x.Slug.Contains(filter.Slug));
            }

            if (!string.IsNullOrEmpty(filter.Description))
            {
                res = res.Where(x => x.Description.Contains(filter.Description));
            }

            if (!string.IsNullOrEmpty(filter.Content))
            {
                res = res.Where(x => x.Content.Contains(filter.Content));
            }

            if (filter.IsActived != null)
            {
                res = res.Where((x) => x.IsActived);
            }

            res = res.OrderByDescending(x => x.CreatedAt);
            var count = await res.CountAsync();
            var result = await res.Skip(offset).Take(filter.Limit.Value).ToListAsync();
            return new DataPaging<News> { Data = result, PaginationCount = count };
        }
        catch (Exception ex)
        {
            _logProvider.Error(ex);
            throw new Exception(ex.Message);
        }
    }
}
