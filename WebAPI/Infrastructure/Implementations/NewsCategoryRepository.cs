﻿using Common.Abstractions;
using Microsoft.EntityFrameworkCore;
using Models.Entities;
using WebApi.Infrastructure.Abstractions;
using WebApi.Models.Context;

namespace WebApi.Infrastructure.Implementations;

public class NewsCategoryRepository : RepositoryBase<NewsCategory>, INewsCategoryRepository
{
    public NewsCategoryRepository(ApplicationDbContext context, ILogProvider logProvider, IConfiguration configuration) : base(context, logProvider, configuration)
    {
    }

    public async Task<IEnumerable<NewsCategory>?> GetActiveParentCategories()
    {
        try
        {
            var res = await _context.Set<NewsCategory>().Where(x => string.IsNullOrEmpty(x.ParentId) && x.IsActived).OrderByDescending(x => x.CreatedAt).ToListAsync();
            return res;
        }
        catch (Exception ex)
        {
            _logProvider.Error(ex);
            throw new Exception(ex.Message);
        }
    }
}
