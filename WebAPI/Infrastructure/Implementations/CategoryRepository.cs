﻿using Common.Abstractions;
using Microsoft.EntityFrameworkCore;
using Models.Common;
using Models.Entities;
using Models.Filter;
using Models.ViewModels;
using WebApi.Infrastructure.Abstractions;
using WebApi.Models.Context;

namespace WebApi.Infrastructure.Implementations;

/// <inheritdoc/>
internal class CategoryRepository : RepositoryBase<Category>, ICategoryRepository
{
    /// <inheritdoc/>
    public CategoryRepository(ApplicationDbContext context, ILogProvider logProvider, IConfiguration configuration) : base(context, logProvider, configuration)
    {
    }

    /// <inheritdoc/>
    public async Task<DataPaging<Category>?> FilterDataPaging(CategoryFilter filter)
    {
        try
        {
            filter.Page ??= 0;
            filter.Limit ??= 50;
            int offset = filter.Page.Value * filter.Limit.Value;
            filter.Offset = offset;
            var res = _context.Set<Category>().Where(x => !string.IsNullOrEmpty(x.Id));

            if (!string.IsNullOrEmpty(filter.Name))
            {
                res = res.Where(x => x.Name.Contains(filter.Name));
            }

            if (filter.IsActived != null)
            {
                res = res.Where((x) => x.IsActived);
            }

            res = res.OrderByDescending(x => x.CreatedAt);
            var count = await res.CountAsync();
            var result = await res.Skip(offset).Take(filter.Limit.Value).ToListAsync();
            return new DataPaging<Category> { Data = result, PaginationCount = count };
        }
        catch (Exception ex)
        {
            _logProvider.Error(ex);
            throw new Exception(ex.Message);
        }
    }

    /// <inheritdoc/>
    public async Task<IEnumerable<Category>?> GetActiveChildrenCategoriesByParentId(string parentId)
    {
        try
        {
            var res = await _context.Set<Category>().Where(x => x.ParentId.Equals(parentId)).OrderByDescending(x => x.CreatedAt).ToListAsync();
            return res;
        }
        catch (Exception ex)
        {
            _logProvider.Error(ex);
            throw new Exception(ex.Message);
        }
    }

    /// <inheritdoc/>
    public async Task<IEnumerable<Category>?> GetActiveParentCategories()
    {
        try
        {
            var res = await _context.Set<Category>().Where(x => string.IsNullOrEmpty(x.ParentId) && x.IsActived).OrderByDescending(x => x.CreatedAt).ToListAsync();
            return res;
        }
        catch (Exception ex)
        {
            _logProvider.Error(ex);
            throw new Exception(ex.Message);
        }
    }

    /// <inheritdoc/>
    public async Task<CategoryViewModel?> GetCategoryTreeBySlug(string slug)
    {
        try
        {
            var res = await _context.Set<Category>().FirstOrDefaultAsync(x => x.Slug.Equals(slug) && x.IsActived);

            if (res != null)
            {
                var categoryViewModel = new CategoryViewModel
                {
                    Id = res.Id,
                    Slug = slug,
                    Image = res.Image,
                    Name = res.Name,
                    ParentId = res.ParentId
                };

                var children = await _context.Set<Category>().Where(x => x.ParentId.Equals(res.Id) && x.IsActived).OrderByDescending(x => x.CreatedAt).ToListAsync();
                
                if (children != null && children.Count > 0)
                {
                    var childrenCategoryViewModel = new List<CategoryViewModel>();

                    foreach (var child in children)
                    {
                        var childViewModel = new CategoryViewModel
                        {
                            Id = child.Id,
                            Slug = child.Slug,
                            Image = child.Image,
                            Name = child.Name,
                            ParentId = child.ParentId
                        };

                        childrenCategoryViewModel.Add(childViewModel);
                    }

                    categoryViewModel.Children = childrenCategoryViewModel;
                }

                return categoryViewModel;
            }
        }
        catch (Exception ex)
        {
            _logProvider.Error(ex);
            throw new Exception(ex.Message);
        }

        return null;
    }
}
