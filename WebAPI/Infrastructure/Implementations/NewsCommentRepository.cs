﻿using Common.Abstractions;
using Models.Entities;
using WebApi.Infrastructure.Abstractions;
using WebApi.Models.Context;

namespace WebApi.Infrastructure.Implementations;

public class NewsCommentRepository : RepositoryBase<NewsComment>, INewsCommentRepository
{
    public NewsCommentRepository(ApplicationDbContext context, ILogProvider logProvider, IConfiguration configuration) : base(context, logProvider, configuration)
    {
    }
}
