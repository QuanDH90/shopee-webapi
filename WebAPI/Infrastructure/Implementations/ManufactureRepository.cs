﻿using Common.Abstractions;
using Models.Entities;
using Models.Common;
using Models.Filter;
using Models.ViewModels;
using WebApi.Infrastructure.Abstractions;
using WebApi.Models.Context;
using Microsoft.EntityFrameworkCore;

namespace WebApi.Infrastructure.Implementations;

/// <inheritdoc/>
internal class ManufactureRepository : RepositoryBase<Manufacture>, IManufactureRepository
{
    public ManufactureRepository(ApplicationDbContext context, ILogProvider logProvider, IConfiguration configuration) : base(context, logProvider, configuration)
    {
    }

    /// <inheritdoc/>
    public async Task<DataPaging<Manufacture>?> FilterDataPaging(ManufactureFilter filter)
    {
        try
        {
            filter.Page ??= 0;
            filter.Limit ??= 50;
            int offset = filter.Page.Value * filter.Limit.Value;
            filter.Offset = offset;
            var res = _context.Set<Manufacture>().Where(x => !string.IsNullOrEmpty(x.Id));

            if (!string.IsNullOrEmpty(filter.Name))
            {
                res = res.Where(x => x.Name.Contains(filter.Name));
            }

            if (!string.IsNullOrEmpty(filter.Website))
            {
                res = res.Where(x => x.Website.Contains(filter.Website));
            }

            if (filter.IsActived != null)
            {
                res = res.Where((x) => x.IsActived);
            }

            var count = await res.CountAsync();
            var result = await res.Skip(offset).Take(filter.Limit.Value).ToListAsync();
            return new DataPaging<Manufacture> { Data = result, PaginationCount = count };
        }
        catch (Exception ex)
        {
            _logProvider.Error(ex);
            throw new Exception(ex.Message);
        }
    }

    /// <inheritdoc/>
    public async Task<IEnumerable<Manufacture>?> GetActiveManufactures()
    {
        try
        {
            var res = await _context.Set<Manufacture>().Where(x => x.IsActived).ToListAsync();
            return res;
        }
        catch (Exception ex)
        {
            _logProvider.Error(ex);
            throw new Exception(ex.Message);
        }
    }

    /// <inheritdoc/>
    public async Task<ManufactureViewModel?> GetManufactureBySlug(string slug)
    {
        try
        {
            var res = await _context.Set<Manufacture>().FirstOrDefaultAsync(x => x.Slug.Equals(slug) && x.IsActived);

            if (res != null)
            {
                var manufactureViewModel = new ManufactureViewModel
                {
                    Id = res.Id,
                    Slug = slug,
                    Image = res.Image,
                    Name = res.Name,
                    Website = res.Website
                };

                return manufactureViewModel;
            }
        }
        catch (Exception ex)
        {
            _logProvider.Error(ex);
            throw new Exception(ex.Message);
        }

        return null;
    }
}
