﻿using Models.Entities;

namespace WebApi.Infrastructure.Abstractions;

public interface INewsCommentRepository : IGenericRepository<NewsComment>
{
}
