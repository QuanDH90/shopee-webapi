﻿using Models.Common;
using Models.Entities;
using Models.Filter;

namespace WebApi.Infrastructure.Abstractions;

public interface INewsRepository : IGenericRepository<News>
{
    /// <summary>
    /// FilterDataPagingAsync
    /// </summary>
    /// <param name="filter"></param>
    /// <returns></returns>
    Task<DataPaging<News>?> FilterDataPagingAsync(NewFilter filter);
}
