﻿using Models.Entities;

namespace WebApi.Infrastructure.Abstractions;

public interface INewsCategoryRepository : IGenericRepository<NewsCategory>
{
    /// <summary>
    /// GetActiveCategories
    /// </summary>
    /// <returns></returns>
    Task<IEnumerable<NewsCategory>?> GetActiveParentCategories();
}
