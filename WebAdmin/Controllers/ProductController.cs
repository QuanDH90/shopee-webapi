﻿using Microsoft.AspNetCore.Mvc;
using Common.Abstractions;
using Models.Common;
using Models.Entities;
using Models.Filter;
using Models.ViewModels;
using Models.Request;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.Collections.Generic;

namespace Salvation.Presentation.WebAdmin.Controllers;

[Route("san-pham")]
public class ProductController : Controller
{
    private readonly ILogProvider _logProvider;

    private readonly ICoreApiProvider _coreApiProvider;

    private string? ApiURL { get; set; }

    public ProductController(ILogProvider logProvider, ICoreApiProvider coreApiProvider, IConfigProvider configProvider)
    {
        _logProvider = logProvider;
        _coreApiProvider = coreApiProvider;
        ApiURL = configProvider.GetConfigString("ApiUrl");
    }

    [HttpGet("danh-sach/{page?}/{limit?}")]
    public async Task<IActionResult> Index(int? page, int? limit)
    {
        try
        {
            limit ??= 10;
            page ??= 1;
            var offset = (page - 1) * limit;

            var filter = new ProductPropertyFilter
            {
                Limit = limit,
                Offset = offset,
                Page = page - 1,
            };

            var data = await _coreApiProvider.PostCore<DataPaging<Product>>(ApiURL + "Product/filter", filter, isExactUrl: true);

            if (data != null && data.Data != null)
            {
                var totalRecord = data.Data.PaginationCount;
                ViewBag.TotalRecord = totalRecord;
                var totalPage = totalRecord % limit == 0 ? totalRecord / limit : totalRecord / limit + 1;
                ViewBag.TotalPage = totalPage <= 0 ? 1 : totalPage;
                ViewBag.Products = data.Data.Data;
            }

            var activedCategories = await _coreApiProvider.GetCore<List<CategoryViewModel>>(ApiURL + "category/get-showing-categories", isExactUrl: true);
            ViewBag.ActivedCategories = activedCategories.Data;
            var activedManufactures = await _coreApiProvider.GetCore<List<ManufactureViewModel>>(ApiURL + "manufacture/get-active-manufactures", isExactUrl: true);
            ViewBag.ActivedManufactures = activedManufactures.Data;
            ViewBag.CurrentPage = page;
            ViewBag.Limit = limit;
        }
        catch (Exception ex)
        {
            _logProvider.Error(ex);
        }

        return View();
    }

    [HttpGet("thuoc-tinh/{page?}/{limit?}")]
    public async Task<IActionResult> Property(int? page, int? limit)
    {
        try
        {
            limit ??= 10;
            page ??= 1;
            var offset = (page - 1) * limit;

            var filter = new ProductPropertyFilter
            {
                Limit = limit,
                Offset = offset,
                Page = page - 1,
            };

            var data = await _coreApiProvider.PostCore<DataPaging<ProductProperty>>(ApiURL + "ProductProperty/filter", filter, isExactUrl: true);

            if (data != null && data.Data != null)
            {
                var totalRecord = data.Data.PaginationCount;
                ViewBag.TotalRecord = totalRecord;
                var totalPage = totalRecord % limit == 0 ? totalRecord / limit : totalRecord / limit + 1;
                ViewBag.TotalPage = totalPage <= 0 ? 1 : totalPage;
                ViewBag.ProductProperties = data.Data.Data;
            }

            var activedCategories = await _coreApiProvider.GetCore<List<CategoryViewModel>>(ApiURL + "category/get-showing-categories", isExactUrl: true);
            ViewBag.ActivedCategories = activedCategories.Data;
            ViewBag.CurrentPage = page;
            ViewBag.Limit = limit;
        }
        catch (Exception ex)
        {
            _logProvider.Error(ex);
        }

        return View();
    }

    #region API
    #region Product
    [HttpGet("chi-tiet/{id}")]
    public async Task<IActionResult> ProductDetail(string id)
    {
        try
        {
            var data = await _coreApiProvider.GetCore<Product>(ApiURL + "Product/get/" + id, isExactUrl: true);
            return Ok(data);
        }
        catch (Exception ex)
        {
            _logProvider.Error(ex);
            return BadRequest(ex.Message);
        }
    }

    [HttpPost("tao-moi")]
    public async Task<IActionResult> CreateProduct(Product entity)
    {
        try
        {
            entity.CreatedAt = DateTime.Now;
            var result = await _coreApiProvider.PostCore<string>(ApiURL + "Product/create", entity, isExactUrl: true);
            return Ok(result);
        }
        catch (Exception ex)
        {
            _logProvider.Error(ex);
            return BadRequest(ex.Message);
        }
    }

    [HttpPost("cap-nhat")]
    public async Task<IActionResult> UpdateProduct(Product entity)
    {
        try
        {
            entity.UpdatedAt = DateTime.Now;
            var result = await _coreApiProvider.PutCore<bool>(ApiURL + "Product/update", entity, isExactUrl: true);
            return Ok(result);
        }
        catch (Exception ex)
        {
            _logProvider.Error(ex);
            return BadRequest(ex.Message);
        }
    }

    [HttpGet("xoa/{id}")]
    public async Task<IActionResult> DeleteProduct(string id)
    {
        try
        {
            var result = await _coreApiProvider.GetCore<bool>(ApiURL + "Product/delete/" + id, isExactUrl: true);
            return Ok(result);
        }
        catch (Exception ex)
        {
            _logProvider.Error(ex);
            return BadRequest(ex.Message);
        }
    }

    [HttpPost("cap-nhat-trang-thai")]
    public async Task<IActionResult> UpdateStatusProduct(EntityStatusUpdate entityStatus)
    {
        try
        {
            var result = await _coreApiProvider.PutCore<bool>(ApiURL + "Product/update-status", entityStatus, isExactUrl: true);
            return Ok(result);
        }
        catch (Exception ex)
        {
            _logProvider.Error(ex);
            return BadRequest(ex.Message);
        }
    }

    [HttpPost("tim-kiem")]
    public async Task<IActionResult> SearchProduct(ProductFilter filter)
    {
        try
        {
            filter.Offset = (filter.Page - 1) * filter.Limit;
            filter.Page--;
            var data = await _coreApiProvider.PostCore<DataPaging<Product>>(ApiURL + "Product/filter", filter, isExactUrl: true);

            if (data != null && data.Data != null)
            {
                var totalRecord = data.Data.PaginationCount;
                ViewBag.TotalRecord = totalRecord;
                var totalPage = totalRecord % filter.Limit == 0 ? totalRecord / filter.Limit : totalRecord / filter.Limit + 1;
                ViewBag.TotalPage = totalPage <= 0 ? 1 : totalPage;
                ViewBag.Products = data.Data.Data;
            }

            ViewBag.CurrentPage = filter.Page;
            ViewBag.Limit = filter.Limit;
        }
        catch (Exception ex)
        {
            _logProvider.Error(ex);
        }
        return View();
    }
    #endregion

    #region Product Property
    [HttpGet("thuoc-tinh/chi-tiet/{id}")]
    public async Task<IActionResult> ProductPropertyDetail(string id)
    {
        try
        {
            var data = await _coreApiProvider.GetCore<ProductProperty>(ApiURL + "ProductProperty/get/" + id, isExactUrl: true);
            return Ok(data);
        }
        catch (Exception ex)
        {
            _logProvider.Error(ex);
            return BadRequest(ex.Message);
        }
    }

    [HttpPost("thuoc-tinh/tao-moi")]
    public async Task<IActionResult> CreateProductProperty(ProductProperty entity)
    {
        try
        {
            entity.Id = Guid.NewGuid().ToString();
            entity.CreatedAt = DateTime.Now;
            var result = await _coreApiProvider.PostCore<string>(ApiURL + "ProductProperty/create", entity, isExactUrl: true);
            return Ok(result);
        }
        catch (Exception ex)
        {
            _logProvider.Error(ex);
            return BadRequest(ex.Message);
        }
    }

    [HttpPost("thuoc-tinh/cap-nhat")]
    public async Task<IActionResult> UpdateProductProperty(ProductProperty entity)
    {
        try
        {
            entity.UpdatedAt = DateTime.Now;
            var result = await _coreApiProvider.PutCore<bool>(ApiURL + "ProductProperty/update", entity, isExactUrl: true);
            return Ok(result);
        }
        catch (Exception ex)
        {
            _logProvider.Error(ex);
            return BadRequest(ex.Message);
        }
    }

    [HttpGet("thuoc-tinh/xoa/{id}")]
    public async Task<IActionResult> DeleteProductProperty(string id)
    {
        try
        {
            var result = await _coreApiProvider.GetCore<bool>(ApiURL + "ProductProperty/delete/" + id, isExactUrl: true);
            return Ok(result);
        }
        catch (Exception ex)
        {
            _logProvider.Error(ex);
            return BadRequest(ex.Message);
        }
    }

    [HttpPost("thuoc-tinh/cap-nhat-trang-thai")]
    public async Task<IActionResult> UpdateStatusProductProperty(EntityStatusUpdate entityStatus)
    {
        try
        {
            var result = await _coreApiProvider.PutCore<bool>(ApiURL + "ProductProperty/update-status", entityStatus, isExactUrl: true);
            return Ok(result);
        }
        catch (Exception ex)
        {
            _logProvider.Error(ex);
            return BadRequest(ex.Message);
        }
    }

    [HttpGet("thuoc-tinh/tim-theo-danh-muc/{categoryId}")]
    public async Task<IActionResult> FindProductPropertyByCategoryId(string categoryId)
    {
        try
        {
            var data = await _coreApiProvider.PostCore<DataPaging<ProductProperty>>(ApiURL + "ProductProperty/filter", new { CategoryId = categoryId }, isExactUrl: true);
            return Ok(data.Data?.Data);
        }
        catch (Exception ex)
        {
            _logProvider.Error(ex);
            return BadRequest(ex.Message);
        }
    }

    [HttpPost("thuoc-tinh/tim-gia-tri")]
    public async Task<IActionResult> FindProductDetailValue(ProductDetailFilter filter)
    {
        try
        {
            var data = await _coreApiProvider.PostCore<ProductDetail>(ApiURL + "ProductDetail/find-by-filter", filter, isExactUrl: true);
            return Ok(data);
        }
        catch (Exception ex)
        {
            _logProvider.Error(ex);
            return BadRequest(ex.Message);
        }
    }

    [HttpPost("thuoc-tinh/cap-nhat-du-lieu")]
    public async Task<IActionResult> UpdateProductDetailValue(ProductDetailUpdateRequest request)
    {
        try
        {
            var data = await _coreApiProvider.PostCore<bool>(ApiURL + "ProductDetail/update-data", request, isExactUrl: true);
            return Ok(data);
        }
        catch (Exception ex)
        {
            _logProvider.Error(ex);
            return BadRequest(ex.Message);
        }
    }
    #endregion

    #region ProductImage
    [HttpGet("hinh-anh/tim-theo-san-pham/{productId}")]
    public async Task<IActionResult> FindProductImageByProductId(string productId)
    {
        try
        {
            var data = await _coreApiProvider.GetCore<List<ProductImage>>(ApiURL + "ProductImage/find-by-product-id/" + productId, isExactUrl: true);
            return Ok(data);
        }
        catch (Exception ex)
        {
            _logProvider.Error(ex);
            return BadRequest(ex.Message);
        }
    }

    [HttpGet("hinh-anh/xoa-hinh-anh/{id}")]
    public async Task<IActionResult> DeleteProductImage(string id)
    {
        try
        {
            var data = await _coreApiProvider.GetCore<bool>(ApiURL + "ProductImage/delete/" + id, isExactUrl: true);
            return Ok(data);
        }
        catch (Exception ex)
        {
            _logProvider.Error(ex);
            return BadRequest(ex.Message);
        }
    }

    [HttpPost("hinh-anh/cap-nhat-du-lieu")]
    public async Task<IActionResult> UpdateProductImageValue(ProductImage request)
    {
        try
        {
            var data = await _coreApiProvider.PostCore<bool>(ApiURL + "ProductImage/update-data", request, isExactUrl: true);
            return Ok(data);
        }
        catch (Exception ex)
        {
            _logProvider.Error(ex);
            return BadRequest(ex.Message);
        }
    }
    #endregion
    #endregion
}
