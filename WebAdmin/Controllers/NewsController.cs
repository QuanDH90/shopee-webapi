﻿using Common.Abstractions;
using Microsoft.AspNetCore.Mvc;
using Models.Common;
using Models.Entities;
using Models.Filter;
using static System.Runtime.InteropServices.JavaScript.JSType;

namespace WebAdmin.Controllers;

public class NewsController : Controller
{
    private readonly ILogProvider _logProvider;

    private readonly ICoreApiProvider _coreApiProvider;

    private string? ApiURL { get; set; }

    public NewsController(ILogProvider logProvider, ICoreApiProvider coreApiProvider, IConfigProvider configProvider)
    {
        _logProvider = logProvider;
        _coreApiProvider = coreApiProvider;
        ApiURL = configProvider.GetConfigString("ApiUrl");
    }

    [HttpGet("/tin-tuc/{page?}/{limit?}")]
    public async Task<IActionResult> Index(int? page, int? limit)
    {
        try
        {
            limit ??= 10;
            page ??= 1;
            var offset = (page - 1) * limit;

            var filter = new NewFilter
            {
                Limit = limit,
                Offset = offset,
                Page = page - 1
            };

            var newsList = await _coreApiProvider.PostCore<DataPaging<News>?>(ApiURL + "News/filter-data-paging", filter, isExactUrl: true);
            if (newsList != null && newsList.Data != null)
            {
                var totalRecord = newsList.Data.PaginationCount;
                ViewBag.TotalRecord = totalRecord;
                var totalPage = totalRecord % limit == 0 ? totalRecord / limit : totalRecord / limit + 1;
                ViewBag.TotalPage = totalPage;
                ViewBag.NewsList = newsList.Data.Data;
            }

            ViewBag.CurrentPage = page;
            ViewBag.Limit = limit;
        }
        catch (Exception ex)
        {
            _logProvider.Error(ex);
        }

        return View();
    }

    [HttpGet("/tin-tuc/chi-tiet/{id?}")]
    public async Task<IActionResult> Detail(string? id)
    {
        if (!string.IsNullOrEmpty(id))
        {
            var news = await _coreApiProvider.GetCore<News>(ApiURL + "News/get-by-id/" + id, isExactUrl: true);
            if (news != null && news.Data != null)
            {
                ViewBag.News = news.Data;
            }
            else
            {
                return RedirectToAction("Index", "News");
            }
        }

        var activedCategories = await _coreApiProvider.GetCore<List<NewsCategory>?>(ApiURL + "NewsCategory/active-parent-categories", isExactUrl: true);
        if (activedCategories != null && activedCategories.Data != null)
        {
            ViewBag.ActivedCategories = activedCategories.Data;
        }

        return View();
    }

    [HttpPost("tin-tuc/luu-tin-tuc")]
    public async Task<IActionResult> Save(News news)
    {
        try
        {
            if (string.IsNullOrEmpty(news.Id))
            {
                news.Id = Guid.NewGuid().ToString();
            }

            var createResult = await _coreApiProvider.PostCore<bool>(ApiURL + "News/create-or-update", news, isExactUrl: true);
            return Ok(createResult);
        }
        catch (Exception ex)
        {
            _logProvider.Error(ex);
            return BadRequest();
        }
    }
}
