﻿using Common.Abstractions;
using Microsoft.AspNetCore.Mvc;
using Models.Enums;
using Models.Request;
using Models.Response;

namespace WebAdmin.Controllers;

public class AccountController : Controller
{
    private readonly ILogProvider _logProvider;

    private readonly ICoreApiProvider _coreApiProvider;

    private readonly ICookieProvider _cookieProvider;

    private string? ApiURL { get; set; }

    public AccountController(ILogProvider logProvider, ICoreApiProvider coreApiProvider, IConfigProvider configProvider, ICookieProvider cookieProvider)
    {
        _logProvider = logProvider;
        _coreApiProvider = coreApiProvider;
        _cookieProvider = cookieProvider;
        ApiURL = configProvider.GetConfigString("ApiUrl");
    }

    public IActionResult Login()
    {
        return View();
    }

    [HttpPost("/do-login")]
    public async Task<IActionResult> DoLogin(LoginRequest request)
    {
        try
        {
            var ipAddress = Request.HttpContext.Connection.RemoteIpAddress;
            request.IpAddress = ipAddress!.ToString();
            request.Type = AccountType.Admin;
            var result = await _coreApiProvider.PostCore<LoginResponse>(ApiURL + "auth/login", request, isExactUrl: true);

            if (result != null && result.Data != null)
            {
                _cookieProvider.Set("accessToken", result!.Data!.AccessToken, 60 * result!.Data!.AccessTokenExpiredAfter);
                _cookieProvider.Set("refreshToken", result!.Data!.RefreshToken, 24 * 3600 * result!.Data!.RefreshTokenExpiredAfter);
            }

            return Ok(result);
        }
        catch (Exception ex)
        {
            _logProvider.Error(ex);
            return BadRequest(ex);
        }
    }
}
