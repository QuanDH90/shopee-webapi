﻿const PageURL = "/tin-tuc";
var myEditor;

$(document).ready(function () {
    ShowImage();
    let NewsContent = $("#NewsContent").val();
    if (NewsContent != "") {
        myEditor.setData(NewsContent);
    }
});

ClassicEditor
    .create(document.querySelector('#editor'), {
        //plugins: [CKFinder, /* ... */],

        //// Enable the insert image button in the toolbar.
        //toolbar: ['uploadImage', /* ... */],

        //ckfinder: {
        //    // Upload the images to the server using the CKFinder QuickUpload command.
        //    uploadUrl: 'https://localhost:7134/plugins/ckfinder/ckfinder.html'
        //}
    })
    .then(editor => {
        console.log('Editor was initialized', editor);
        myEditor = editor;
    })
    .catch(error => {
        console.error(error);
    });

async function CreateOrUpdate() {
    let Id = $("#Id").val();
    let CategoryId = $("#CategoryId").val();
    let Title = $("#Title").val();
    let Slug = $("#Slug").val();
    let Description = $("#Description").val();
    let Content = myEditor.getData();
    let Image = $("#Image").val();
    let Tags = $("#Tags").val();
    let Status = $("#Status").val();
    const data = { Id, CategoryId, Title, Slug, Description, Content, Image, Tags, Status };
    console.log(data);

    const res = await AjaxProvider.Post({
        url: PageURL + "/luu-tin-tuc",
        data: data
    });

    if (res.code < 0) {
        Swal.fire({
            title: "Lỗi",
            text: "Lưu thất bại! Vui lòng kiểm tra lại!",
            icon: "error",
        });
        return;
    } else {
        Swal.fire({
            title: "Thông báo",
            html: "Lưu thành công!",
            icon: "success"
        }).then(() => {
            window.location.href = PageURL;
        });
    }
}

function GenerateSlug() {
    let Title = $("#Title").val();
    $("#Slug").val(stringToSlug(Title));
}

function ShowImage() {
    let Image = $("#Image").val();
    if (Image != "") {
        $("#ImageShow").attr("src", Image)
    }
}

function stringToSlug(str) {
    // Chuyển hết sang chữ thường
    str = str.toLowerCase();

    // xóa dấu
    str = str
        .normalize('NFD') // chuyển chuỗi sang unicode tổ hợp
        .replace(/[\u0300-\u036f]/g, ''); // xóa các ký tự dấu sau khi tách tổ hợp

    // Thay ký tự đĐ
    str = str.replace(/[đĐ]/g, 'd');

    // Xóa ký tự đặc biệt
    str = str.replace(/([^0-9a-z-\s])/g, '');

    // Xóa khoảng trắng thay bằng ký tự -
    str = str.replace(/(\s+)/g, '-');

    // Xóa ký tự - liên tiếp
    str = str.replace(/-+/g, '-');

    // xóa phần dư - ở đầu & cuối
    str = str.replace(/^-+|-+$/g, '');

    // return
    return str;
}

function GenerateTag() {
    let tags = $("#Tags").val();
    let tagArray = tags.split(",");
    let tagHtml = "";
    tagArray.forEach(tag => {
        tagHtml += `<span class="badge badge-primary">${tag}</span> `;
    });
    $("#tag-display").html(tagHtml);
}