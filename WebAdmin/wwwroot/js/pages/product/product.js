﻿let EntityId = null;
let IsUpdateMode = false;
const PageURL = "/san-pham";

$(function () {
    $(".page-link").click(function (e) {
        e.preventDefault();
        let page = $(this).data("page");
        let limit = $("#perpage").val();
        console.log(page, limit);
        window.location.href = `${PageURL}/${page}/${limit}`
    })
})

function CreateModal() {
    IsUpdateMode = false;
    ClearModal();
}

async function Create() {
    let CategoryId = $("#CategoryId").val()
    let ManufactureId = $("#ManufactureId").val()
    let Name = $("#Name").val()
    let Slug = $("#Slug").val()
    let Image = $("#Image").val()
    let Price = $("#Price").val()
    let SalePrice = $("#SalePrice").val()
    let IsActived = $("#IsActived").val()
    let IsDeleted = !IsActived
    EntityId = uuidv4()
    Price = Price.replace(/,/g, "")
    SalePrice = SalePrice.replace(/,/g, "")

    const data = { Id: EntityId, Name, Slug, CategoryId, ManufactureId, FeatureImage: Image, Price, SalePrice, IsActived, IsDeleted }

    const res = await AjaxProvider.Post({
        url: PageURL + "/tao-moi",
        data: data
    });

    if (res.code < 0) {
        Swal.fire({
            title: "Lỗi",
            text: "Thêm mới thất bại! Vui lòng kiểm tra lại!",
            icon: "error",
        });
        return;
    } else {
        Swal.fire({
            title: "Thông báo",
            html: "Thêm mới thành công!",
            icon: "success"
        }).then(() => {
            location.reload();
        });
    }
}

async function Edit(Id) {
    IsUpdateMode = true;
    ClearModal();
    EntityId = Id;

    const res = await AjaxProvider.Get({
        url: PageURL + "/chi-tiet/" + Id,
    });

    console.log(res)

    if (res.code == 0) {
        $("#CategoryId").val(res.data.categoryId)
        $("#ManufactureId").val(res.data.manufactureId)
        $("#Name").val(res.data.name)
        $("#Slug").val(res.data.slug)
        $("#Image").val(res.data.featureImage)
        $("#Price").val(res.data.price)
        let sale = res.data.salePrice ?? 0
        $("#SalePrice").val(sale)
        $("#IsActived").val(res.data.isActived + "")
        formatMoneyNumber('Price')
        formatMoneyNumber('SalePrice')
        ShowImage()
    }

    GetProductImages();
    ChangeCategory();
    GetProductImage();
}

async function Update(Id) {
    let CategoryId = $("#CategoryId").val();
    let ManufactureId = $("#ManufactureId").val()
    let Name = $("#Name").val()
    let Slug = $("#Slug").val()
    let Image = $("#Image").val()
    let Price = $("#Price").val()
    let SalePrice = $("#SalePrice").val()
    let IsActived = $("#IsActived").val();
    let IsDeleted = !IsActived;
    Price = Price.replace(/,/g, "")
    SalePrice = SalePrice.replace(/,/g, "")

    const data = { Id, Name, Slug, CategoryId, ManufactureId, FeatureImage: Image, Price, SalePrice, IsActived, IsDeleted }

    const res = await AjaxProvider.Post({
        url: PageURL + "/cap-nhat",
        data: data
    });

    if (res.code < 0) {
        Swal.fire({
            title: "Lỗi",
            text: "Cập nhật thất bại! Vui lòng kiểm tra lại!",
            icon: "error",
        });
        return;
    } else {
        Swal.fire({
            title: "Thông báo",
            html: "Cập nhật thành công!",
            icon: "success"
        }).then(() => {
            location.reload();
        });
    }
}

function CreateOrUpdate() {
    if (EntityId != null) {
        Update(EntityId)
    } else {
        Create()
    }
}

function ClearModal() {
    ProductPropertyId = null;
    $("#CategoryId").val("")
    $("#ManufactureId").val("")
    $("#Name").val("")
    $("#Slug").val("")
    $("#Image").val("")
    $("#Price").val("")
    $("#IsActived").val("")
    $("#property-table").html("");
    $("#ImageShow").attr("src", "")
    $("#image-table").html("")
}

function GenerateSlug() {
    let Name = $("#Name").val();
    $("#Slug").val(stringToSlug(Name));
}

function stringToSlug(str) {
    // Chuyển hết sang chữ thường
    str = str.toLowerCase();

    // xóa dấu
    str = str
        .normalize('NFD') // chuyển chuỗi sang unicode tổ hợp
        .replace(/[\u0300-\u036f]/g, ''); // xóa các ký tự dấu sau khi tách tổ hợp

    // Thay ký tự đĐ
    str = str.replace(/[đĐ]/g, 'd');

    // Xóa ký tự đặc biệt
    str = str.replace(/([^0-9a-z-\s])/g, '');

    // Xóa khoảng trắng thay bằng ký tự -
    str = str.replace(/(\s+)/g, '-');

    // Xóa ký tự - liên tiếp
    str = str.replace(/-+/g, '-');

    // xóa phần dư - ở đầu & cuối
    str = str.replace(/^-+|-+$/g, '');

    // return
    return str;
}

async function Active(Id, status) {
    const category = { Id, IsActived: status }

    const res = await AjaxProvider.Post({
        url: PageURL + "/cap-nhat-trang-thai",
        data: category
    });

    if (res.code < 0) {
        Swal.fire({
            title: "Lỗi",
            text: "Cập nhật thất bại! Vui lòng kiểm tra lại!",
            icon: "error",
        });
        return;
    } else {
        Swal.fire({
            title: "Thông báo",
            html: "Cập nhật thành công!",
            icon: "success"
        }).then(function () {
            location.reload()
        });
    }
}

async function Delete(Id) {
    const res = await AjaxProvider.Get({
        url: PageURL + "/xoa/" + Id
    });

    if (res.code < 0) {
        Swal.fire({
            title: "Lỗi",
            text: "Xoá thất bại! Vui lòng kiểm tra lại!",
            icon: "error",
        });
        return;
    } else {
        Swal.fire({
            title: "Thông báo",
            html: "Xoá thành công!",
            icon: "success"
        }).then(function () {
            location.reload()
        });
    }
}

function ShowImage() {
    let Image = $("#Image").val();
    if (Image != "") {
        $("#ImageShow").attr("src", Image)
    }
}

async function Search() {
    let Name = $("#SearchName").val();
    let UpPrice = $("#UpPrice").val();
    let DownPrice = $("#DownPrice").val();
    let Limit = $("#perpage").val();
    let Page = $("#Page").val();

    const data = { Name, UpPrice, DownPrice, Limit, Page };

    const res = await AjaxProvider.Post({
        url: PageURL + "/tim-kiem",
        data: data
    });

    if (res) {
        $("#page-wrapper").html(res);
    }
}

async function GetProductImages() {

}

async function ChangeCategory() {
    let CategoryId = $("#CategoryId").val();
    const res = await AjaxProvider.Get({
        url: PageURL + "/thuoc-tinh/tim-theo-danh-muc/" + CategoryId
    })

    if (res) {
        let htmlStr = "";

        for (let i = 0; i < res.length; i++) {
            let id = !IsUpdateMode ? uuidv4() : res[i].id
            let value = !IsUpdateMode ? "" : await GetProductDetailValue(id)

            htmlStr += `<tr id=${id}>
                <td>
                    <span data-id="${id}" id="ProductProperty-${id}">${res[i].name}</span>
                </td>
                <td><input type="text" class="form-control" value="${value}" id="Value-${id}" /></td>
                <td>
                    <button class="btn btn-sm btn-warning" onclick="UpdateProperty('${id}')">Update</button>
                </td>
            </tr>`
        }

        $("#property-table").html(htmlStr);
    }
}

async function GetProductDetailValue(id) {
    let result = ""
    const data = { ProductPropertyId: id, ProductId: EntityId }
    const res = await AjaxProvider.Post({
        url: PageURL + "/thuoc-tinh/tim-gia-tri",
        data: data
    })

    if (res.code == 0 && res.data) {
        result = res.data.value
    }

    return result
}

async function UpdateProperty(id) {
    let ProductPropertyId = $("#ProductProperty-" + id).data("id")
    let Value = $("#Value-" + id).val()
    const data = { Id: id, ProductId: EntityId, ProductPropertyId, Value }
    console.log(data)

    const res = await AjaxProvider.Post({
        url: PageURL + "/thuoc-tinh/cap-nhat-du-lieu",
        data: data
    })

    if (res.code < 0) {
        Swal.fire({
            title: "Lỗi",
            text: "Cập nhật thất bại! Vui lòng kiểm tra lại!",
            icon: "error",
        })
    } else {
        Swal.fire({
            title: "Thông báo",
            html: "Cập nhật thành công!",
            icon: "success"
        })
    }
}

function uuidv4() {
    return "10000000-1000-4000-8000-100000000000".replace(/[018]/g, c =>
        (+c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> +c / 4).toString(16)
    );
}

function ShowImageRow(id) {
    let Image = $("#ProductImage-" + id).val();
    if (Image != "") {
        $("#ProductImage-Show-" + id).attr({ "src": Image, "width": "150px" })
    }
}

function AddImageRow() {
    let id = uuidv4()
    let htmlStr = `<tr>
                        <td>
                            <input type="text" class="form-control" id="ProductImage-${id}" onkeyup="ShowImageRow('${id}')" />
                        </td>
                        <td style="width: 150px;">
                            <img id="ProductImage-Show-${id}" />
                        </td>
                        <td>
                            <button class="btn btn-sm btn-primary" onclick="RemoveImageRow('${id}')">Remove</button>
                            <button class="btn btn-sm btn-info" onclick="UpdateImageRow('${id}')">Update</button>
                        </td>
                    </tr>`
    $("#image-table").append(htmlStr)
}

async function GetProductImage() {
    const res = await AjaxProvider.Get({
        url: PageURL + "/hinh-anh/tim-theo-san-pham/" + EntityId
    })

    if (res.code == 0 && res.data.length > 0) {
        let htmlStr = ""

        for (let i = 0; i < res.data.length; i++) {
            id = res.data[i].id
            image = res.data[i].image
            htmlStr += `<tr id="${id}">
                        <td>
                            <input type="text" class="form-control" id="ProductImage-${id}" value="${image}" onkeyup="ShowImageRow('${id}')" />
                        </td>
                        <td>
                            <img style="width: 150px;" id="ProductImage-Show-${id}" src="${image}" />
                        </td>
                        <td>
                            <button class="btn btn-sm btn-primary" onclick="RemoveImageRow('${id}')">Remove</button>
                            <button class="btn btn-sm btn-info" onclick="UpdateImageRow('${id}')">Update</button>
                        </td>
                    </tr>`
        }

        $("#image-table").html(htmlStr)
    }
}

async function UpdateImageRow(id) {
    let ProductImage = $("#ProductImage-" + id).val()
    const data = { Id: id, ProductId: EntityId, Image: ProductImage }

    const res = await AjaxProvider.Post({
        url: PageURL + "/hinh-anh/cap-nhat-du-lieu",
        data: data
    })

    if (res.code < 0) {
        Swal.fire({
            title: "Lỗi",
            text: "Cập nhật thất bại! Vui lòng kiểm tra lại!",
            icon: "error",
        })
    } else {
        Swal.fire({
            title: "Thông báo",
            html: "Cập nhật thành công!",
            icon: "success"
        })
    }
}

async function RemoveImageRow(id) {
    const res = await AjaxProvider.Get({
        url: PageURL + "/hinh-anh/xoa-hinh-anh/" + id
    })

    if (res.code < 0) {
        Swal.fire({
            title: "Lỗi",
            text: "Cập nhật thất bại! Vui lòng kiểm tra lại!",
            icon: "error",
        })
    } else {
        Swal.fire({
            title: "Thông báo",
            html: "Cập nhật thành công!",
            icon: "success"
        }).then(function () {
            $("#" + id).remove()
        })
    }
}

function formatMoneyNumber(id) {
    let number = $("#" + id).val()
    number = number.replace(/,/g, "")
    let result = parseInt(number).toLocaleString()
    $("#" + id).val(result)
}