﻿let rememberMe = false;

async function login() {
    const email = $("#email").val();
    const password = $("#password").val();

    if (!email || !password) {
        Swal.fire({
            title: "Lỗi",
            text: "Email and password is required!",
            icon: "error",
        });
        return;
    };

    const res = await AjaxProvider.Post({
        url: "/do-login",
        data: { email, password, rememberMe }
    });

    if (res.code < 0) {
        Swal.fire({
            title: "Lỗi",
            text: res.message,
            icon: "error",
        });
        return;
    } else {
        Swal.fire({
            title: "Thông báo",
            html: res.message,
            icon: "success"
        }).then(() => {
            window.location.href = '/';
        });
    }
}

function clickRememberMe() {
    rememberMe = !rememberMe;
}