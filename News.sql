use SalvationStore
go

create table NewsCategory (
	Id varchar(200) not null primary key,
	ParentId varchar(200),
	Name nvarchar(200) unique,
	Slug varchar(200),
	Image varchar(200),
	IsActived bit not null default 1,
	CreatedAt datetime default getdate(),
	CreatedBy varchar(200),
	UpdatedAt datetime,
	UpdatedBy varchar(200),
	DeletedAt datetime,
	DeletedBy varchar(200),
	IsDeleted bit not null default 0
)
go

create table News (
	Id varchar(200) not null primary key,
	CategoryId varchar(200),
	CategoryName nvarchar(200),
	Title nvarchar(200) unique,
	Slug varchar(200),
	Description nvarchar(500),
	Image varchar(200),
	Content ntext,
	Tags nvarchar(500),
	IsActived bit not null default 1,
	CreatedAt datetime default getdate(),
	CreatedBy varchar(200),
	UpdatedAt datetime,
	UpdatedBy varchar(200),
	DeletedAt datetime,
	DeletedBy varchar(200),
	IsDeleted bit not null default 0
)
go

create table NewsComment (
	Id varchar(200) not null primary key,
	NewsId varchar(200),
	NewsTitle nvarchar(200),
	CommentId varchar(200),
	CommentParentId varchar(200),
	CommentTime datetime,
	CommentContent nvarchar(500),
	UserId varchar(200),
	FullName nvarchar(200),
	Email varchar(200),
	IsActived bit not null default 1,
	CreatedAt datetime default getdate(),
	CreatedBy varchar(200),
	UpdatedAt datetime,
	UpdatedBy varchar(200),
	DeletedAt datetime,
	DeletedBy varchar(200),
	IsDeleted bit not null default 0
)
go