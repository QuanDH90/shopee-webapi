﻿using Microsoft.AspNetCore.Mvc;
using Common.Abstractions;
using Models.ViewModels;
using Models.Filter;
using Models.Common;
using Models.Entities;
using Models.Enums;

namespace Salvation.Presentation.WebApp.Controllers;

/// <summary>
/// ProductController
/// </summary>
public class ProductController : Controller
{
    /// <summary>
    /// ICoreApiProvider
    /// </summary>
    private readonly ICoreApiProvider _coreApiProvider;

    /// <summary>
    /// ILogProvider
    /// </summary>
    private readonly ILogProvider _logProvider;

    private string? ApiURL { get; set; }

    public ProductController(ICoreApiProvider coreApiProvider, ILogProvider logProvider, IConfigProvider configProvider)
    {
        _coreApiProvider = coreApiProvider;
        _logProvider = logProvider;
        ApiURL = configProvider.GetConfigString("ApiUrl");
    }

    [Route("san-pham")]
    public async Task<IActionResult> Index(
        [FromQuery] string? category,
        [FromQuery] string? manufacture,
        [FromQuery] string? productName,
        [FromQuery] int? upPrice,
        [FromQuery] int? downPrice,
        [FromQuery] ProductOrder? productOrder,
        [FromQuery] int? limit,
        [FromQuery] int? page,
        [FromQuery] bool? isGridView = true)
    {
        try
        {
            var categoryId = "";
            var manufactureId = "";
            var categoryList = new List<CategoryViewModel>();
            var manufactureList = new List<ManufactureViewModel>();
            #region Category and Manufacture
            var categoriesResult = await _coreApiProvider.GetCore<List<CategoryViewModel>>(ApiURL + "category/get-parent-active-categories", isExactUrl: true);

            if (categoriesResult != null && categoriesResult.Code == 0 && categoriesResult.Data != null)
            {
                categoryList = categoriesResult.Data;
            }

            var manufacturesResult = await _coreApiProvider.GetCore<List<ManufactureViewModel>>(ApiURL + "manufacture/get-active-manufactures", isExactUrl: true);

            if (manufacturesResult != null && manufacturesResult.Code == 0 && manufacturesResult.Data != null)
            {
                manufactureList = manufacturesResult.Data;
            }
            #endregion

            #region Parameters
            if (!string.IsNullOrEmpty(category))
            {
                var categoryFind = await _coreApiProvider.GetCore<CategoryViewModel>(ApiURL + "category/get-category-tree-by-slug/" + category, isExactUrl: true);

                if (categoryFind != null && categoryFind?.Code == 0 && categoryFind?.Data != null)
                {
                    ViewBag.CategoryViewModel = categoryFind?.Data;
                    categoryId = categoryFind?.Data.Id;
                }
            }

            if (!string.IsNullOrEmpty(manufacture))
            {
                var manufactureFind = await _coreApiProvider.GetCore<ManufactureViewModel>(ApiURL + "Manufacture/get-manufacture-by-slug/" + manufacture, isExactUrl: true);

                if (manufactureFind != null && manufactureFind?.Code == 0 && manufactureFind?.Data != null)
                {
                    ViewBag.ManufactureViewModel = manufactureFind?.Data;
                    manufactureId = manufactureFind?.Data.Id;
                }
            }

            ViewBag.CurrentPage = page;
            ViewBag.Limit = limit;
            ViewBag.ProductName = productName;
            ViewBag.UpPrice = upPrice;
            ViewBag.DownPrice = downPrice;
            ViewBag.IsGridView = isGridView;
            ViewBag.ProductOrder = productOrder;
            #endregion

            #region Product
            limit ??= 12;
            page ??= 1;
            var offset = (page - 1) * limit;

            var filter = new ProductFilter
            {
                Limit = limit,
                Offset = offset,
                Page = page - 1,
                CategoryId = categoryId,
                ManufactureId = manufactureId,
                Name = productName,
                DownPrice = downPrice <= 0 ? null : downPrice,
                UpPrice = upPrice <= 0 ? null : upPrice,
            };

            var productData = await _coreApiProvider.PostCore<DataPaging<Product>>(ApiURL + "Product/filter", filter, isExactUrl: true);

            if (productData != null && productData.Code == 0 && productData.Data != null)
            {
                var totalRecord = productData.Data.PaginationCount;
                ViewBag.TotalRecord = totalRecord;
                var totalPage = totalRecord % limit == 0 ? totalRecord / limit : totalRecord / limit + 1;
                ViewBag.TotalPage = totalPage <= 0 ? 1 : totalPage;
                var listProduct = productData.Data.Data;
                ViewBag.Products = listProduct;
                if (!string.IsNullOrEmpty(category))
                {
                    var manufactureListTemp = new List<ManufactureViewModel>();
                    var manufactureIdList = listProduct.Select(x => x.ManufactureId).Distinct().ToList();
                    if (manufactureIdList != null && manufactureIdList.Count > 0)
                    {
                        foreach (var item in manufactureIdList)
                        {
                            manufactureListTemp.AddRange(manufactureList.Where(x => x.Id == item).ToList());
                        }
                    }

                    manufactureList = manufactureListTemp.Distinct().ToList();
                }

                if (!string.IsNullOrEmpty(manufacture))
                {
                    var categoryListTemp = new List<CategoryViewModel>();
                    var categoryIdList = listProduct.Select(x => x.CategoryId).Distinct().ToList();
                    if (categoryIdList != null && categoryIdList.Count > 0)
                    {
                        foreach (var item in categoryIdList)
                        {
                            categoryListTemp.AddRange(categoryList.Where(x => x.Id == item).ToList());
                        }
                    }

                    categoryList = categoryListTemp.Distinct().ToList();
                }
            }
            #endregion
            ViewBag.Categories = categoryList;
            ViewBag.Manufactures = manufactureList;
        }
        catch (Exception ex)
        {
            _logProvider.Error(ex);
        }

        return View();
    }

    [Route("/san-pham/chi-tiet/{slug}")]
    public async Task<IActionResult> Detail(string slug)
    {
        var productData = await _coreApiProvider.GetCore<Product?>(ApiURL + "Product/get-by-slug/" + slug, isExactUrl: true);

        if (productData != null && productData.Code == 0 && productData.Data != null)
        {
            var category = await _coreApiProvider.GetCore<Category>(ApiURL + "category/get-category-by-id/" + productData.Data.CategoryId, isExactUrl: true);
            if (category != null)
            {
                ViewBag.Category = category.Data;
            }

            var manufacture = await _coreApiProvider.GetCore<Manufacture>(ApiURL + "Manufacture/get-manufacture-by-id/" + productData.Data.ManufactureId, isExactUrl: true);
            if (manufacture != null)
            {
                ViewBag.Manufacture = manufacture.Data;
            }

            ViewBag.Product = productData.Data;
            var productImages = await _coreApiProvider.GetCore<List<ProductImage>?>(ApiURL + "ProductImage/find-by-product-id/" + productData.Data.Id, isExactUrl: true);

            if (productImages != null && productImages.Code == 0 && productImages.Data != null)
            {
                ViewBag.ProductImages = productImages.Data;
            }

            var productDetails = await _coreApiProvider.GetCore<List<ProductDetail>?>(ApiURL + "ProductDetail/find-by-product-id/" + productData.Data.Id, isExactUrl: true);

            if (productDetails != null && productDetails.Code == 0 && productDetails.Data != null)
            {
                ViewBag.ProductDetails = productDetails.Data;
            }
        }

        return View();
    }

    [Route("/xay-dung-cau-hinh")]
    public IActionResult Build()
    {
        return View();
    }

    #region Category
    [Route("/get-category-tree/{id}")]
    public async Task<IActionResult> GetCategoryTree(string id)
    {
        var data = await _coreApiProvider.GetCore<CategoryViewModel?>(ApiURL + "Category/get-category-tree/" + id, isExactUrl: true);
        return Ok(data);
    }

    [Route("/get-manufacture/{id}")]
    public async Task<IActionResult> GetManufacture(string id)
    {
        var data = await _coreApiProvider.GetCore<Manufacture?>(ApiURL + "Manufacture/get-manufacture-by-id/" + id, isExactUrl: true);
        return Ok(data);
    }
    #endregion
}
