﻿using Microsoft.AspNetCore.Mvc;

namespace WebApp.Controllers;

public class NewsController : Controller
{
    [HttpGet("/tin-tuc")]
    public IActionResult Index()
    {
        return View();
    }

    [HttpGet("/tin-tuc/chi-tiet/{slug}")]
    public IActionResult Detail(string slug)
    {
        return View();
    }
}
