﻿let queryStringObj = {
    category: "",
    manufacture: "",
    productName: "",
    upPrice: 0,
    downPrice: 0,
    productOrder: 0,
    limit: 12,
    page: 1,
    isGridView: true
}

queryStringObj = getAllQueryParameters()

async function clickOnCategory(id) {
    window.event.preventDefault();
    queryStringObj.category = id;
    ChangeDisplayUrl()
}

async function clickOnManufacture(id) {
    window.event.preventDefault();
    queryStringObj.manufacture = id
    ChangeDisplayUrl()
}

function ChangeDisplayUrl() {
    let params = objectToQueryString(queryStringObj);
    const currentURL = window.location.origin + window.location.pathname + "?" + params;
    //console.log(currentURL)
    window.location.href = currentURL;
}

function getAllQueryParameters() {
    const queryString = window.location.search;
    const urlParams = new URLSearchParams(queryString);
    const params = {};
    for (const [key, value] of urlParams.entries()) {
        params[key] = value;
    }
    return params;
}

function filterByPrice() {
    let DownPrice = $("#DownPrice").val();
    let UpPrice = $("#UpPrice").val();
    DownPrice = DownPrice.replace(/,/g, "")
    UpPrice = UpPrice.replace(/,/g, "")
    DownPrice = parseInt(DownPrice);
    UpPrice = parseInt(UpPrice);
    console.log(DownPrice, UpPrice)

    if (DownPrice == 0 && UpPrice == 0) {
        return;
    }

    queryStringObj.downPrice = DownPrice;
    queryStringObj.upPrice = UpPrice;
    ChangeDisplayUrl()
}

function formatMoneyNumber(id) {
    let number = $("#" + id).val()
    number = number.replace(/,/g, "")
    let result = parseInt(number).toLocaleString()
    $("#" + id).val(result)
}

function changeDisplayGridView(status) {
    window.event.preventDefault();
    queryStringObj.isGridView = status
    ChangeDisplayUrl()
}

function objectToQueryString(obj) {
    const params = new URLSearchParams();
    for (const key in obj) {
        if (obj.hasOwnProperty(key)) {
            params.append(key, obj[key]);
        }
    }
    return params.toString();
}

function changePerPage(id) {
    window.event.preventDefault();
    let perpage = $("#" + id).val();
    queryStringObj.limit = perpage
    ChangeDisplayUrl()
}